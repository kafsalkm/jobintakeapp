﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [DBTableName(DALConstants.GENERICCODE_TABLE)]
    public class GenericCode
    {
        [DBField(DALConstants.ID, Identity = true)]
        public Int64 Id { get; set; }

        [DBField(DALConstants.CODE)]
        public string Code { get; set; }

        [DBField(DALConstants.DESCRIPTION)]
        public string Description { get; set; }

        [DBField(DALConstants.IS_ACTIVE)]
        public bool IsActive { get; set; }
    }
}
