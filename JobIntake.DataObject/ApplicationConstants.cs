﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    public sealed class ApplicationConstants
    {
        #region SQL Exception Numbers

        public const int SQL_EXCEPTION_NUMBER_DELETE = 547;

        #endregion

        #region Common Fields

        // This is used for the format of the Date Time to avoid conflict with the format in SQL serevr
        //yyyy  	four-digit year
        //MM 	    two-digit month (01=January, etc.)
        //dd 	    two-digit day of month (01 through 31)
        //HH 	    two digits of hour (00 through 23)(24 hour representation)
        //mm 	    two digits of minute (00 through 59)
        //ss 	    two digits of second (00 through 59)
        //s 	    one or more digits representing a decimal fraction of a second
        //public const string DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";


        //Standard datetime format that will work irrespective of the 
        //datetime format specified in tht SQL server settings
        //public const string DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss:fff";

        //public const string DEFAULT_DATE_TIME_SQL = "01/01/1753 00:00:00";

        public const string ENCRYPTION_PASS_KEY = "EncPassKey";
        public const string DEFAULT_DATE_TIME_SQL = "1753-01-01 00:00:00";

        public const string SHORT_DATE_TIME_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// The value used to represent a null decimal value
        /// </summary>
        public const decimal NullDecimal = decimal.MinValue;

        /// <summary>
        /// The value used to represent a null double value
        /// </summary>
        public const double NullDouble = double.MinValue;

        /// <summary>
        /// The value used to represent a null int value
        /// </summary>
        public const int NullInt = int.MinValue;

        /// <summary>
        /// The value used to represent a null long value
        /// </summary>
        public const long NullLong = long.MinValue;

        /// <summary>
        /// The value used to represent a null float value
        /// </summary>
        public const float NullFloat = float.MinValue;

        /// <summary>
        /// The value used to represent a null string value
        /// </summary>
        public const string NullString = null;

        /// <summary>
        /// he value used to represent a null short value
        /// </summary>
        public const short NullShort = short.MinValue;

        //constants for the common fields of most of the tables.
        public const string CONFIG_FOLDER_PATH = "ConfigFolderPath";
        public const string ID = "Id";
        public const string CODE = "Code";
        public const string MESSAGE = "Message";
        public const string NAME = "Name";
        public const string LAST_UPDATED = "LastUpdated";
        public const string COMPANY = "Company";
        public const string DESCRIPTION = "Description";
        public const string ENTITY = "Entity";
        public const string ADD = "Add";
        public const string EDIT = "Edit";
        public const string OVERVIEW = "Overview";
        public const string COPY = "Copy";
        public const string CANCEL = "Cancel";
        public const string COPY_OF = "Copy of ";
        public const string DATE_TIME_FORMAT = "dd/mm/yyyy";
        public const string MONTHLY = "MONTHLY";
        public const string WEEKLY = "WEEKLY";
        public const string BIWEEKLY = "BIWEEKLY";
        public const string WARNING = "Warning";
        public const string APPLICABLE_ENTITY = "ApplicableEntity";
        public const string LOAD = "Load";
        public const string ACTION = "Action";
        public const string COLUMNS_LIST = "ColumnsList";
        public const string ROW_MODIFIED = "RowModified";
        public const string DETAILS = "Details";
        public const string EVENT_HANDLED = "EventHandled";
        public const string PERIOD = "Period";
        public const string COMMENTS = "Comments";
        public const string REFERENCE_DATE = "ReferenceDate";
        public const string PROJECT = "Project";
        public const string CLIENT_CONTRACT = "ClientContract";
        public const string APPROVALSHEET_DETAILS = "ApprovalSheetDetails";
        public const string TITLE = "Title";
        public const string GENDER = "Gender";
        public const string MARITAL_STATUS = "MaritalStatus";
        public const string SEQUENCE = "Sequence";
        public const string QUANTITIES = "Quantities";
        public const string RATES = "Rates";
        public const string IS_EDITED_BY_USER = "IsEditedByUser";
        public const string READ_ONLY = "ReadOnly";
        public const string IS_ACTIVE = "IsActive";
        public const string COMPANY_ID = "CompanyId";
        public const string COUNTRY_CODE_INDICATOR = "CountryCodeIndicator";
        public const string COUNTRY_CODE = "countrycode";
        public const string GROUP_CODE_INDICATOR = "GroupCodeIndicator";
        public const string DISPLAY_MEMBER = "DisplayMember";
        public const string VALUE_MEMBER = "ValueMember";
        public const string TEMPLATE_REPLACE_CHARACTER = "~";
        public const string LETTERTEMPLATEQUERY = "@Query@";
        public const string RULE_SET_CATEGORY = "RuleSetCategory";
        public const string RULE_SET_ID = "RuleSetId";
        public const string BRANCHNAME = "BranchName";

        public const string USER_NAME = "UserName";
        public const string CLIENT = "Client";
        public const string HELPDESK = "HelpDesk";
        public const string SESSION_DETAILS = "SessionDetails";

        public const string GET_SQL_DATE = "GetDate()";
        public const string LETTERTEMPLATE_CTCBREAKUP = "@BreakUp@";
        public const string CANDIDATEDETAILS = "CandidateDetails";
        public const string LETTERCODE = "~LetterCode~";
        public const string OFFER_LETTER_LINK_KEY = "~OfferLetterLink~";
        public const string APPOINTMENT_LETTER_LINK_KEY = "~AppointmentLetterLink~";

        public const string MEMBERTYPE = "MemberType";

        public const string RATE_SET_DETAILS = "RatesetDetails";

        public const string CANDIDATE_DETAILS = "CandidateDetails";
        public const string ELC_ACTION = "ELCAction";
        public const string NAVIGATION_ACTION = "NavigationAction";
        #endregion

        #region LogConstants
        public const string ENTITY_LOG_DETAILS = "EntityLogDetails";
        public const string ACTIVITY_DETAILS = "ActivityDetails";
        public const string ACTIVITY_LIST = "ActivityList";
        public const string AUDIT_DETAILS = "AuditDetails";
        public const string ENTITY_LOG_TABLE = "EntityLog";
        public const string ACTIVITY = "Activity";
        public const string ACTIVITY_ID = "ActivityId";
        public const string AUDIT = "Audit";
        public const string FIELD = "Field";
        public const string OLD_VALUES = "OldValues";
        public const string NEW_VALUES = "NewValues";
        public const string ENTITY_TYPE = "EntityType";
        public const string ENTITY_CODE = "EntityCode";
        public const string ENTITY_ID = "EntityId";
        public const string DATE_TIME = "DateTime";
        public const string USER_ID = "UserId";
        public const string APPLICATION_ID = "ApplicationId";
        public const string BASE_MESSAGE = "BaseMessage";
        public const string EXTENDED_MESSAGE = "ExtendedMessage";
        public const string LOG_ID = "LogId";
        public const string DB_TRANSACTION = "DbTransaction";
        public const string MANAGER_USER_ID = "ManagerUserId";
        #endregion

        #region DataType in String

        public const string DT_LONG = "System.Int64";
        public const string DT_BOOL = "System.Boolean";
        public const string DT_STRING = "System.String";
        public const string DT_DECIMAL = "System.Decimal";
        public const string DT_INT = "System.Int32";
        public const string DT_SHORT = "System.Int16";
        public const string DT_DATETIME = "System.DateTime";
        public const string DT_NAMESPACE = "RandstadDigital.DataObject";
        public const string DT_REFERENCEDETAILS = " RandstadDigital.DataObject.ReferenceDetails";
        public const string DT_RULEDETAILS = "RandstadDigital.DataObject.RuleDetails";

        #endregion

        #region Databaseselection

        public const string DEFAULT_CONNECTION_KEY_NAME = "DefaultConnectionKeyName";
        public const string INDIACOMPANY_CONNECTION_KEY_NAME = "IndiaCompany";

        #endregion
    }
}
