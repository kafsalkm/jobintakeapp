﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;

namespace JobIntake.DataObject
{
    [Serializable]
    public abstract class BaseDataObject : ICloneable
    {
        /// <summary>
        /// Clone the object, and returning a reference to a cloned object.
        /// </summary>
        /// <returns>Reference to the new cloned object</returns>
        public object Clone()
        {
            //First we create an instance of this specific type.
            object newObject = Activator.CreateInstance(this.GetType());
            //object tempbject = Activator.CreateInstance(this.GetType());

            //We get the array of fields for the new type instance.
            PropertyInfo[] properties = newObject.GetType().GetProperties();
            properties = newObject.GetType().GetProperties();

            int i = 0;

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                //We query if the fiels support the ICloneable interface.
                Type ICloneType = pi.PropertyType.GetInterface("ICloneable", true);

                if (ICloneType != null)
                {
                    //Getting the ICloneable interface from the object.
                    ICloneable IClone = (ICloneable)pi.GetValue(this, null);
                    if (IClone != null)
                    {
                        //We use the clone method to set the new value to the field.
                        properties[i].SetValue(newObject, IClone.Clone(), null);
                    }
                }
                else
                {
                    // If the field doesn't support the ICloneable 
                    // interface then just set it.
                    properties[i].SetValue(newObject, pi.GetValue(this, null), null);
                }

                //Now we check if the object support the 
                //IEnumerable interface, so if it does

                //we need to enumerate all its items and check if 
                //they support the ICloneable interface.

                Type IEnumerableType = pi.PropertyType.GetInterface
                                ("IEnumerable", true);
                if (IEnumerableType != null)
                {
                    //Get the IEnumerable interface from the field.
                    IEnumerable IEnum = (IEnumerable)pi.GetValue(this, null);

                    if (IEnum == null)
                    {
                        i++;
                        continue;
                    }

                    //This version support the IList and the 
                    //IDictionary interfaces to iterate on collections.
                    Type IListType = properties[i].PropertyType.GetInterface
                                        ("IList", true);
                    Type IDicType = properties[i].PropertyType.GetInterface
                                        ("IDictionary", true);

                    int j = 0;
                    if (IListType != null)
                    {
                        //Getting the IList interface.
                        IList list = (IList)properties[i].GetValue(newObject, null);
                        IList tempList = null;
                        foreach (object obj in IEnum)
                        {
                            //Checking to see if the current item 
                            //support the ICloneable interface.
                            ICloneType = obj.GetType().
                                GetInterface("ICloneable", true);

                            if (ICloneType != null)
                            {
                                //If it does support the ICloneable interface, 
                                //we use it to set the clone of
                                //the object in the list.
                                ICloneable clone = (ICloneable)obj;
                                if (clone != null)
                                {
                                    if (tempList == null)
                                    {
                                        tempList = (IList)CreateListInstance(obj.GetType());
                                    }
                                    tempList.Add(clone.Clone());
                                    //list[j] = clone.Clone();
                                }
                            }

                            //NOTE: If the item in the list is not 
                            //support the ICloneable interface then in the 
                            //cloned list this item will be the same 
                            //item as in the original list
                            //(as long as this type is a reference type).
                            j++;
                        }

                        list = tempList;
                    }
                    else if (IDicType != null)
                    {
                        //Getting the dictionary interface.

                        IDictionary dic = (IDictionary)properties[i].
                                            GetValue(newObject, null);
                        j = 0;

                        foreach (DictionaryEntry de in IEnum)
                        {
                            //Checking to see if the item 
                            //support the ICloneable interface.

                            ICloneType = de.Value.GetType().
                                GetInterface("ICloneable", true);

                            if (ICloneType != null)
                            {
                                ICloneable clone = (ICloneable)de.Value;

                                dic[de.Key] = clone.Clone();
                            }
                            j++;
                        }
                    }
                }
                i++;
            }
            return newObject;
        }

        public object CreateListInstance(Type type)
        {
            return this.CreateInstance(this.BuildList(type));
        }

        public object CreateInstance(Type type)
        {
            return Activator.CreateInstance(type);
        }

        private Type BuildList(Type t)
        {
            return Type.GetType("System.Collections.Generic.List`1[[" + t.FullName + ", " + t.Assembly.ToString() + "]]");
        }
    }
}
