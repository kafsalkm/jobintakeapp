﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [DBTableName(DALConstants.JOB_TABLE)]
    public class Job
    {
        [DBField(DALConstants.ID, Identity = true)]
        public Int64 Id { get; set; }

        [DBField(DALConstants.JOB_TITLE)]
        public string JobTitle { get; set; }

        [DBField(DALConstants.NO_REQUIRED)]
        public Int64 NoRequired { get; set; }

        [DBField(DALConstants.NO_SOFAR)]
        public Int64 NoSoFar { get; set; }

        [DBField(DALConstants.SALARY_FROM)]
        public Int64 SalaryFrom { get; set; }

        [DBField(DALConstants.SALARY_TO)]
        public Int64 SalaryTo { get; set; }

        [DBField(DALConstants.LOCATION)]
        public Int64 Location { get; set; }

        [DBField(DALConstants.JOB_SPEC)]
        public string JobSpecification { get; set; }

        [DBField(DALConstants.CLIENT_ID)]
        public Int64 ClientId { get; set; }

        [DBField(DALConstants.STATUS)]
        public Int64 Status { get; set; }

        [DBField(DALConstants.OVERALLEXP)]
        public Int64 OverallExp { get; set; }

        [DBField(DALConstants.RELAENTEXP)]
        public Int64 RelaventExp { get; set; }

        public List<Skill> lstSkills { get; set; }

        public string LocationName { get; set; }
    }
}
