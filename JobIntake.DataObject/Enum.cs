﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
        public enum MessageType
        {
            CONFIRMATION,
            INFORMATION,
            SUCCESS,
            ERROR,
            WARNING
        }
    
}
