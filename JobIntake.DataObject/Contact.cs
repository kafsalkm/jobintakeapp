﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [DBTableName(DALConstants.CONTACT_TABLE)]
    public class Contact
    {
        [DBField(DALConstants.ID, Identity=true)]
        public Int64 Id { get; set; }

        [DBField(DALConstants.FIRST_NAME)]
        public string FirstName { get; set; }

        [DBField(DALConstants.LAST_NAME)]
        public string LastName { get; set; }
        
        [DBField(DALConstants.DESIGNATION)]
        public string Designation { get; set; }

        [DBField(DALConstants.EMAIL_ID)]
        public string EmialId { get; set; }

        [DBField(DALConstants.MOBILE_NO)]
        public string MobileNo { get; set; }
        
        [DBField(DALConstants.IS_ACTIVE)]
        public bool IsActive { get; set; }
    }
}
