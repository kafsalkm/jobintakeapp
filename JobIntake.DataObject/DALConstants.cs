﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    public class DALConstants
    {
        #region Table Names
        public const string CLIENT_TABLE = "ClientMaster";
        public const string CONTACT_TABLE = "ContactMaster";
        public const string JOB_TABLE = "JobMaster";
        public const string SKILL_TABLE = "JobSkills";
        public const string GENERICCODE_TABLE = "GenericCodes";
        public const string DBLOCK_TABLE = "DBLock";
        public const string ENTITY_NAME = "EntityName";
        #endregion

        #region General Litterals
        public const string ID = "Id";
        public const string NAME = "Name";
        public const string CLIENT_NAME = "ClientName";
        public const string CLIENT_ID = "ClientId";
        public const string LOCATION = "Location";
        public const string ADDRESS1 = "AddressLine1";
        public const string ADDRESS2 = "AddressLine2";
        public const string CONTACT = "Contact";
        public const string IS_ACTIVE = "Is_Active";
        public const string FIRST_NAME = "Firstname";
        public const string LAST_NAME = "Lastname";
        public const string DESIGNATION = "Designation";
        public const string EMAIL_ID = "EmailId";
        public const string MOBILE_NO = "MobileNo";
        public const string JOB_ID = "JobId";
        public const string SKILL = "Skill";
        public const string CODE = "Code";
        public const string DESCRIPTION = "Description";
        public const string JOB_TITLE = "JobTitle";
        public const string NO_REQUIRED = "NoRequired";
        public const string NO_SOFAR = "NoSoFar";
        public const string SALARY_FROM = "SalaryFrom";
        public const string SALARY_TO = "SalaryTo";
        public const string JOB_SPEC = "JobSpecification";
        public const string STATUS = "Status";
        public const string OVERALLEXP = "OverallExperience";
        public const string RELAENTEXP = "RelaventExperience";
        public const string LAST_UPDATED = "LastUpdatedBy";
        public const string LASTUPDATEDON = "LastUpdatedOn";
        public const string END_DATE = "EndDate";
        public const string COMMENT = "Comment";
        public const string COMMENTS = "Comments";
        public const string CREATEDON = "CreatedOn";
        public const string CREATED_BY = "CreatedBy";
        public const string LOCKED_BY = "LockedBy";
        #endregion
    }

    
    /// <summary>
    /// Contains a listing of constants used throughout the application
    /// </summary>
    public sealed class Constants
    {
        // This is used for the format of the Date Time to avoid conflict with the format in SQL serevr
        //yyyy  	four-digit year
        //MM 	    two-digit month (01=January, etc.)
        //dd 	    two-digit day of month (01 through 31)
        //HH 	    two digits of hour (00 through 23)(24 hour representation)
        //mm 	    two digits of minute (00 through 59)
        //ss 	    two digits of second (00 through 59)
        //s 	    one or more digits representing a decimal fraction of a second
        //public const string DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";


        //Standard datetime format that will work irrespective of the 
        //datetime format specified in tht SQL server settings
        public const string DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss:fff";

        //public const string DEFAULT_DATE_TIME_SQL = "01/01/1753 00:00:00";

        public const string DEFAULT_DATE_TIME_SQL = "1753-01-01 00:00:00";

        public const string SHORT_DATE_TIME_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// The value used to represent a null decimal value
        /// </summary>
        public const decimal NullDecimal = decimal.MinValue;

        /// <summary>
        /// The value used to represent a null double value
        /// </summary>
        public const double NullDouble = double.MinValue;

        /// <summary>
        /// The value used to represent a null int value
        /// </summary>
        public const int NullInt = int.MinValue;

        /// <summary>
        /// The value used to represent a null long value
        /// </summary>
        public const long NullLong = long.MinValue;

        /// <summary>
        /// The value used to represent a null float value
        /// </summary>
        public const float NullFloat = float.MinValue;

        /// <summary>
        /// The value used to represent a null string value
        /// </summary>
        public const string NullString = null;

        /// <summary>
        /// he value used to represent a null short value
        /// </summary>
        public const short NullShort = short.MinValue;


    }
}
