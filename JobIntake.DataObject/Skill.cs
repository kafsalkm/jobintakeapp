﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [DBTableName(DALConstants.SKILL_TABLE)]
    public class Skill
    {
        [DBField(DALConstants.ID, Identity = true)]
        public Int64 Id { get; set; }

        [DBField(DALConstants.JOB_ID)]
        public Int64 JobId { get; set; }

        [DBField(DALConstants.SKILL)]
        public Int64 SkillId { get; set; }
    }
}
