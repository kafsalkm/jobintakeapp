﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    public class ErrorDetails
    {
        public ErrorDetails()
        { }

        public ErrorDetails(string errMsg)
        {
            this.ErrorMessage = errMsg;
        }

        public DateTime Date { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorTag { get; set; }
        public long Id { get; set; }
        public MessageType MessageType { get; set; }
    }
}
