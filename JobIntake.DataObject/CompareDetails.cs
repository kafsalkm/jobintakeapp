﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [Serializable]
    public class CompareDetails : Attribute
    {
        #region Private Variables

        string category;
        string displayName;
        Type associatedType;
        bool primaryKey;
        bool considerField;

        #endregion

        #region Properties

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public Type AssociatedType
        {
            get { return associatedType; }
            set { associatedType = value; }
        }

        public bool PrimaryKey
        {
            get { return primaryKey; }
            set { primaryKey = value; }
        }

        public bool ConsiderField
        {
            get { return considerField; }
            set { considerField = value; }
        }

        #endregion

        #region Constructor

        public CompareDetails()
        {
            this.Category = string.Empty;
            this.DisplayName = string.Empty;
            this.primaryKey = false;
            this.AssociatedType = null;
            this.ConsiderField = true;
        }

        public CompareDetails(string displayName)
        {
            this.DisplayName = displayName;
            this.Category = string.Empty;
            this.primaryKey = false;
            this.AssociatedType = null;
            this.ConsiderField = true;
        }

        public CompareDetails(Type associatdType)
        {
            this.Category = string.Empty;
            this.DisplayName = string.Empty;
            this.primaryKey = false;
            this.AssociatedType = associatdType;
            this.ConsiderField = true;
        }

        public CompareDetails(string category,
                                string displayName)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.primaryKey = false;
            this.AssociatedType = null;
            this.ConsiderField = true;
        }

        public CompareDetails(string category,
                                 bool primaryKey)
        {
            this.Category = category;
            this.DisplayName = string.Empty;
            this.primaryKey = primaryKey;
            this.AssociatedType = null;
            this.ConsiderField = true;
        }


        public CompareDetails(string displayName,
                                 bool primaryKey,
                                 bool considerField)
        {
            this.Category = string.Empty;
            this.DisplayName = displayName;
            this.primaryKey = primaryKey;
            this.AssociatedType = null;
            this.ConsiderField = considerField;
        }

        public CompareDetails(string category,
                               string displayName,
                               bool primaryKey)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.primaryKey = primaryKey;
            this.AssociatedType = null;
            this.ConsiderField = true;
        }

        public CompareDetails(string category,
                              string displayName,
                              bool primaryKey,
                              bool considerField)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.primaryKey = primaryKey;
            this.AssociatedType = null;
            this.ConsiderField = considerField;
        }

        public CompareDetails(string category,
                                Type associatedType)
        {
            this.Category = category;
            this.DisplayName = string.Empty;
            this.primaryKey = false;
            this.AssociatedType = associatedType;
            this.ConsiderField = true;
        }

        public CompareDetails(string category,
                                string displayName,
                                Type associatedType)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.primaryKey = false;
            this.AssociatedType = associatedType;
            this.ConsiderField = true;
        }

        public CompareDetails(string category,
                                string displayName,
                                Type associatedType,
                                bool primaryKey)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.primaryKey = primaryKey;
            this.AssociatedType = associatedType;
            this.ConsiderField = true;
        }

        public CompareDetails(bool considerField)
        {
            this.DisplayName = string.Empty;
            this.Category = string.Empty;
            this.primaryKey = false;
            this.AssociatedType = null;
            this.ConsiderField = considerField;
        }

        #endregion
    }

    [Serializable]
    public class CompareDBDetails : Attribute
    {
        #region Private Variables

        string category;
        string displayName;
        Type associatedType;
        string tableName;
        string fieldName;
        string conditionFieldName;

        #endregion

        #region Properties

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public Type AssociatedType
        {
            get { return associatedType; }
            set { associatedType = value; }
        }

        public string TableName
        {
            get { return tableName; }
            set { tableName = value; }
        }

        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }

        public string ConditionFieldName
        {
            get { return conditionFieldName; }
            set { conditionFieldName = value; }
        }

        #endregion

        #region Constructor

        public CompareDBDetails(string category,
                                    Type associatedType)
        {
            this.Category = category;
            this.AssociatedType = associatedType;
            this.TableName = string.Empty;
            this.FieldName = string.Empty;
            this.ConditionFieldName = string.Empty;
            this.DisplayName = string.Empty;
        }

        public CompareDBDetails(string category,
                                    string tableName,
                                    string fieldName,
                                    string conditionFieldName)
        {
            this.Category = category;
            this.TableName = tableName;
            this.FieldName = fieldName;
            this.ConditionFieldName = conditionFieldName;
            this.DisplayName = string.Empty;
            this.AssociatedType = null;
        }

        public CompareDBDetails(string category,
                                    string displayName,
                                    string tableName,
                                    string fieldName,
                                    string conditionFieldName)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.TableName = tableName;
            this.FieldName = fieldName;
            this.ConditionFieldName = conditionFieldName;
            this.AssociatedType = null;
        }

        public CompareDBDetails(string category,
                                    string displayName,
                                    Type associatedType,
                                    string tableName,
                                    string fieldName,
                                    string conditionFieldName)
        {
            this.Category = category;
            this.DisplayName = displayName;
            this.TableName = tableName;
            this.FieldName = fieldName;
            this.ConditionFieldName = conditionFieldName;
            this.AssociatedType = associatedType;
        }

        #endregion
    }
}
