﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    public class ObjectResult
    {
        public int status { get; set; }
        public String message { get; set; }
        public Object results { get; set; }
    }
}
