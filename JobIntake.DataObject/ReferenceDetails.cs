﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{

        public class ReferenceDetails : BaseDataObject
        {
            #region Private Variables

            private long id;
            private string code;
            private string name;
            private string description;

            #endregion

            #region Constructor

            public ReferenceDetails()
            {
                //Default Constructor
            }

            public ReferenceDetails(long id, string code)
            {
                this.id = id;
                this.code = code;
            }

            public ReferenceDetails(ReferenceDetails referenceDetails)
            {
                if (referenceDetails != null)
                {
                    this.id = referenceDetails.id;
                    this.code = referenceDetails.code;
                }
            }

            #endregion

            #region Properties

            public long Id
            {
                get { return id; }
                set { id = value; }
            }

            [CompareDetails(false)]
            public string Code
            {
                get { return code; }
                set { code = value; }
            }

            [CompareDetails(false)]
            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }

            [CompareDetails(false)]
            public string Description
            {
                get { return description; }
                set { description = value; }
            }

            #endregion

            #region Public Methods

            public bool Equals(ReferenceDetails referenceDtls)
            {
                if (referenceDtls == null)
                {
                    return false;
                }

                if (this.id != referenceDtls.Id)
                {
                    return false;
                }

                if (this.code != referenceDtls.Code)
                {
                    return false;
                }

                if (this.name != referenceDtls.Name)
                {
                    return false;
                }

                if (this.description != referenceDtls.Description)
                {
                    return false;
                }

                return true;
            }

            #endregion
        }
}
