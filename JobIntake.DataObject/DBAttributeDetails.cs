﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
        public enum DefaultValueType
        {
            None,
            SQLFunction,
            BuiltInValueType,
            GetFromSessionDetails
        }

        public class DBField : Attribute
        {
            #region Private Variable

            private string name;
            private bool identity;
            private object defaultValue;
            private DefaultValueType defaultValueType;

            #endregion

            #region Constructor

            public DBField()
            {
                //Default constructor
            }

            public DBField(string name)
            {
                this.Name = name;
            }

            #endregion

            #region Properties

            public string Name
            {
                get { return this.name; }
                set { this.name = value; }
            }

            public bool Identity
            {
                get { return this.identity; }
                set { this.identity = value; }
            }

            public DefaultValueType DefaultValueType
            {
                get { return defaultValueType; }
                set { this.defaultValueType = value; }
            }

            public object DefaultValue
            {
                get { return defaultValue; }
                set { this.defaultValue = value; }
            }

            #endregion
        }

        public class DBTableName : Attribute
        {
            #region Private Variable

            private string fieldName;

            #endregion

            #region Properties

            public DBTableName(string fieldName)
            {
                this.fieldName = fieldName;
            }

            public string FieldName
            {
                get { return this.fieldName; }
            }

            #endregion
        }

}
