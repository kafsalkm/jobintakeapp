﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    [DBTableName(DALConstants.CLIENT_TABLE)]
    public class Client
    {
        [DBField(DALConstants.ID, Identity = true)]
        public Int64 Id { get; set; }

        [DBField(DALConstants.CLIENT_NAME)]
        public string ClientName { get; set; }

        [DBField(DALConstants.ADDRESS1)]
        public string AddressLine1 { get; set; }

        [DBField(DALConstants.ADDRESS2)]
        public string AddressLine2 { get; set; }

        [DBField(DALConstants.LOCATION)]
        public Int64 Location { get; set; }

        [DBField(DALConstants.CONTACT)]
        public Int64 Contact { get; set; }

        [DBField(DALConstants.IS_ACTIVE)]
        public bool IsActive { get; set; }

        public Contact ContactDetails { get; set; }
    }
}
