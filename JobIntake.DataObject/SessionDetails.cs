﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobIntake.DataObject
{
    public class SessionDetails : ICloneable
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long RoleId { get; set; }
        public bool IsAdministrator { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClientName { get; set; }
        public string DBName { get; set; }
        public string Query { get; set; }
        public string RoleName { get; set; }

        /// <summary>
        /// Implements Clone method
        /// </summary>
        /// <returns> Clone object of the given RuleItem</returns>
        public object Clone()
        {
            SessionDetails objSessionDetails = new SessionDetails();
            objSessionDetails.UserId = UserId;
            objSessionDetails.UserName = UserName;
            objSessionDetails.RoleId = RoleId;
            objSessionDetails.IsAdministrator = IsAdministrator;
            objSessionDetails.FirstName = FirstName;
            objSessionDetails.LastName = LastName;
            objSessionDetails.ClientName = ClientName;
            objSessionDetails.DBName = DBName;
            objSessionDetails.Query = Query;
            objSessionDetails.RoleName = RoleName;
            return (object)objSessionDetails;
        }
    }
}
