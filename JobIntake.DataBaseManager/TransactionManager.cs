﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JobIntake.DataObject;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace JobIntake.DataBaseManager
{
    public class TransactionManager
    {
        #region Private Variables
        private SqlConnection sqlConn = null;
        #endregion

        #region Transaction Methods
        /// <summary>
        /// This overload function provides Sql Transaction based on the session details
        /// This method provides default isolation level as "ReadComitted"
        /// </summary>
        /// <param name="sesDetails">Information about Session Details like ClientDB Name and UserID</param>
        /// <returns>New SQL Transaction</returns>
        public IDbTransaction BeginTransaction(SessionDetails sesDetails)
        {
            IDbTransaction sqlTxn = null;
            sqlConn = GetConnection(sesDetails);
            if (sqlConn != null)
            {
                sqlConn.Open();
                sqlTxn = sqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
            }
            return sqlTxn;
        }

        /// <summary>
        /// This overload function provides the option to set the Isolationlevel base on the SysInterface request
        /// </summary>
        /// <param name="isoLevel">isolation level can be ReadCommited, readuncommit...</param>
        /// <param name="sesDetails">Session Details like ClientDB Name and UserID</param>
        /// <returns>New SQL Transaction</returns>
        public IDbTransaction BeginTransaction(IsolationLevel isoLevel,
                                                        SessionDetails sesDetails)
        {
            IDbTransaction sqlTxn = null;
            sqlConn = GetConnection(sesDetails);
            if (sqlConn != null)
            {
                sqlConn.Open();
                sqlTxn = sqlConn.BeginTransaction(isoLevel);
            }
            return sqlTxn;
        }

        /// <summary>
        /// This overload method provides the option to define userdefined transaction name, isolation level
        /// and Session Details
        /// </summary>
        /// <param name="TransactionName">Name of the Transaction</param>
        /// <param name="isoLevel">Isolation Level</param>
        /// <param name="sesDetails">Session Details like ClientDB Name and User ID</param>
        /// <returns>New SQL Transaction</returns>
        public IDbTransaction BeginTransaction(string transactionName,
                                                        IsolationLevel isoLevel,
                                                        SessionDetails sesDetails)
        {
            IDbTransaction sqlTxn = null;
            sqlConn = GetConnection(sesDetails);
            if (sqlConn != null)
            {
                sqlConn.Open();
                sqlTxn = sqlConn.BeginTransaction(isoLevel, transactionName);
            }
            return sqlTxn;
        }

        /// <summary>
        /// Commits the current transaction.
        /// </summary>
        public bool CommitTransaction(IDbTransaction sqlTxn)
        {
            if (sqlConn != null && sqlConn.State == ConnectionState.Open && sqlTxn != null)
            {
                sqlTxn.Commit();
                sqlConn.Close();
                sqlTxn.Dispose();
                sqlTxn = default(IDbTransaction);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Rolls back the current transaction.
        /// </summary>
        public bool RollbackTransaction(IDbTransaction sqlTxn)
        {
            if (sqlConn != null && sqlConn.State == ConnectionState.Open && sqlTxn != null)
            {
                sqlTxn.Rollback();
                sqlConn.Close();
                sqlTxn.Dispose();
                sqlTxn = default(IDbTransaction);
                return true;
            }
            return false;
        }
        #endregion

        #region Connection Methods
        /// <summary>
        /// Get the Connection String from Configuration Manager
        /// </summary>
        /// <returns></returns>
        private static string GetConnectionString(SessionDetails sesDetails)
        {
            string childDatabaseName = sesDetails.DBName;

            if (childDatabaseName.Contains(";"))
            {
                return sesDetails.DBName;
            }

            string connectionString = string.Empty;


            try
            {
                connectionString = ConfigurationManager.ConnectionStrings[sesDetails.DBName].ConnectionString;
                return connectionString;
            }
            catch (Exception keyEx)
            {
                throw new Exception("Key not found: " + sesDetails.DBName);
            }


            string serverName = ConfigurationManager.AppSettings["Server Name"];
            string userId = ConfigurationManager.AppSettings["UserID"];
            string password = ConfigurationManager.AppSettings["Password"];
            string connectionTimeOut = ConfigurationManager.AppSettings["Connection TimeOut"];
            string pooling = ConfigurationManager.AppSettings["Pooling"];

            connectionString = "Server=" + serverName + ";Database=" + childDatabaseName + ";User ID=" + userId + ";Password=" + password + ";Pooling=" + pooling + ";Connection Timeout = " + connectionTimeOut + ";";
            string maxPoolSize = ConfigurationManager.AppSettings["Max Pool Size"];

            if (!String.IsNullOrEmpty(maxPoolSize))
            {
                connectionString = connectionString + "Max Pool Size =" + maxPoolSize + ";";
            }

            return connectionString;  //ConfigurationManager.ConnectionStrings[childDatabaseCodeConnectionString].ConnectionString;
        }

        /// <summary>
        /// This method is used to Get the New Connection
        /// </summary>
        /// <param name="sesDetails">Session details to build the connection string</param>
        /// <returns>New Sql Connection</returns>
        public static SqlConnection GetConnection(SessionDetails sesDetails)
        {
            return new SqlConnection(GetConnectionString(sesDetails));
        }
        #endregion
    }
}
