﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Reflection;
using System.Configuration;
using JobIntake.DataObject;
namespace JobIntake.DataBaseManager
{
    public class BaseDataAccessLayer : BaseDAL
    {
        #region Constructor
        public BaseDataAccessLayer(IDbTransaction sqlTxn)
            : base(sqlTxn)
        {
        }
        #endregion

        #region Common Methods

        /// <summary>
        /// This Method is used to conver the ParameterArray into the SQLParam List
        /// </summary>
        /// <param name="paramList">Parameter Array</param>
        /// <returns>SqlParameter Array</returns>
        public SqlParameter[] ConvertoArray(ArrayList paramList)
        {
            SqlParameter[] paramArray = new SqlParameter[paramList.Count];
            paramArray = (SqlParameter[])paramList.ToArray(typeof(SqlParameter));
            return paramArray;
        }

        #endregion

        #region Generic Fill Method

        /// <summary>
        /// This method is used to Convert the Datareader to List of object for the specified Type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="reader">Data Reader</param>
        /// <returns>List of Objects</returns>
        public List<T> ConvertDataReaderToObject<T>(IDataReader reader) where T : new()
        {
            #region Null Check for Parameters

            if (reader == null)
            {
                return null;
            }

            #endregion

            #region Get Type

            T typeobj = new T();
            Type type = typeobj.GetType();

            #endregion

            #region Get List of Properties

            Dictionary<DBField, PropertyInfo> propList = new Dictionary<DBField, PropertyInfo>();

            foreach (PropertyInfo propInfo in type.GetProperties())
            {
                foreach (DBField dbField in propInfo.GetCustomAttributes(typeof(DBField), true))
                {
                    propList[dbField] = propInfo;
                }
            }

            if (propList == null || propList.Count == 0)
            {
                return null;
            }

            #endregion

            #region Create Object for each row in the Reader

            List<T> objList = new List<T>();

            while (reader.Read())
            {
                T obj = new T();
                T filledObj = CreateObject<T>(reader, type, obj, propList);
                if (filledObj != null)
                {
                    objList.Add(filledObj);
                }
            }

            #endregion

            #region Close reader and Return the List of Objects

            reader.Close();
            return objList;

            #endregion
        }

        /// <summary>
        /// This method is used to Convert the Datareader to List of object for the specified Type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="reader">Data Reader</param>
        /// <returns>List of Objects</returns>
        public object ConvertDataReaderToObject(object typeobj, IDataReader reader)
        {
            #region Null Check for Parameters

            if (reader == null)
            {
                return null;
            }

            #endregion

            #region Get Type

            Type type = typeobj.GetType();

            #endregion

            #region Get List of Properties

            Dictionary<DBField, PropertyInfo> propList = new Dictionary<DBField, PropertyInfo>();

            foreach (PropertyInfo propInfo in type.GetProperties())
            {
                foreach (DBField dbField in propInfo.GetCustomAttributes(typeof(DBField), true))
                {
                    propList[dbField] = propInfo;
                }
            }

            if (propList == null || propList.Count == 0)
            {
                return null;
            }

            #endregion

            #region Create Object for each row in the Reader

            while (reader.Read())
            {
                #region Create Object

                foreach (KeyValuePair<DBField, PropertyInfo> pair in propList)
                {
                    try
                    {
                        DBField dbField = pair.Key;
                        string dbFieldName = dbField.Name;
                        PropertyInfo pi = pair.Value;

                        string propertyName = pi.Name;
                        Type propertyType = pi.PropertyType;
                        object value = DBNull.Value;

                        try
                        {
                            value = reader[dbFieldName];
                        }
                        catch (System.IndexOutOfRangeException)
                        {
                            continue;
                        }

                        if (propertyType == typeof(ReferenceDetails))
                        {
                            value = new ReferenceDetails((long)value, string.Empty);
                        }
                        else if (propertyType.BaseType == typeof(Enum))
                        {
                            value = Enum.Parse(propertyType, value.ToString());
                        }
                        else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            if (propertyType.GetGenericArguments()[0].BaseType == typeof(Enum))
                            {
                                if (value != DBNull.Value)
                                {
                                    value = Enum.Parse(propertyType.GetGenericArguments()[0], value.ToString());
                                }
                            }
                        }
                        if (value == DBNull.Value)
                        {
                            value = null;
                        }

                        type.GetProperty(propertyName).SetValue(typeobj, value, null);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }

                #endregion
            }

            #endregion

            #region Close reader and Return the List of Objects

            reader.Close();
            return typeobj;

            #endregion
        }

        /// <summary>
        /// This method is used to create a object for the reader of a specified Type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="reader">Data Reader</param>
        /// <param name="type">Type of the object</param>
        /// <param name="obj">object</param>
        /// <param name="propList">List of Properties</param>
        /// <returns>List of Objects</returns>
        private T CreateObject<T>(IDataReader reader,
                                            Type type,
                                            object obj,
                                            Dictionary<DBField, PropertyInfo> propList) where T : new()
        {
            #region Null check for Parameters

            if (reader == null || obj == null || propList == null)
            {
                return default(T);
            }

            #endregion

            #region Create Object

            foreach (KeyValuePair<DBField, PropertyInfo> pair in propList)
            {
                try
                {
                    DBField dbField = pair.Key;
                    string dbFieldName = dbField.Name;
                    PropertyInfo pi = pair.Value;

                    string propertyName = pi.Name;
                    Type propertyType = pi.PropertyType;
                    object value = DBNull.Value;

                    try
                    {
                        value = reader[dbFieldName];
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        continue;
                    }

                    if (propertyType == typeof(ReferenceDetails))
                    {
                        value = new ReferenceDetails((long)value, string.Empty);
                    }
                    else if (propertyType.BaseType == typeof(Enum))
                    {
                        value = Enum.Parse(propertyType, value.ToString());
                    }
                    else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        if (propertyType.GetGenericArguments()[0].BaseType == typeof(Enum))
                        {
                            if (value != DBNull.Value)
                            {
                                value = Enum.Parse(propertyType.GetGenericArguments()[0], value.ToString());
                            }
                        }
                    }
                    if (value == DBNull.Value)
                    {
                        value = null;
                    }

                    type.GetProperty(propertyName).SetValue(obj, value, null);
                }
                catch (Exception ex)
                {
                    return default(T);
                }
            }

            #endregion

            #region Return

            if (obj != null)
            {
                return (T)obj;
            }
            else
            {
                return default(T);
            }
            #endregion
        }

        #endregion

        #region Generic Insert Method

        /// <summary>
        /// This method is used to Insert the record in the table
        /// /// Object Array 1. Number of Rows Affected, 2. Inserted Id, 3. Error Details
        /// </summary>
        /// <param name="obj">Object</param>a        
        /// <param name="lovEventId">Log Event Id</param>
        /// <returns>Object Array 1. Number of Rows Affected, 2. Inserted Id, 3. Error Details</returns>
        public object[] InsertRecord(object obj,
                                        int logEventId)
        {
            #region Get Type

            Type type = obj.GetType();

            #endregion

            #region Get Properties

            Dictionary<DBField, PropertyInfo> propList = GetObjectProperties(type, obj);

            #endregion

            #region Get Table Name

            string tableName = GetTableName(obj);

            #endregion

            #region Insert Record

            return InsertRecord(tableName, obj, type, propList, logEventId);

            #endregion
        }

        /// <summary>
        /// This method is used to Insert the record in the table
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="tableName">Table Name</param>
        /// <param name="logEventId">Log EventId</param>
        /// <param name="type">Type of Object</param>
        /// <param name="propList">List of Properties</param>
        /// <returns>Object Array 1. Number of Rows Affected, 2. Inserted Id, 3. Error Details</returns>
        private object[] InsertRecord(string tableName,
                                        object obj,
                                        Type type,
                                        Dictionary<DBField, PropertyInfo> propList,
                                        int logEventId)
        {
            #region Local Variable Initialization

            object[] returnObj = new object[3];
            returnObj[0] = 0;
            returnObj[1] = 0;
            returnObj[2] = null;

            #endregion

            #region Null check for Parameters

            if (obj == null || string.IsNullOrEmpty(tableName))
            {
                returnObj[2] = new ErrorDetails("Table Name/Object is Null or Empty");
                return returnObj;
            }

            if (propList == null || propList.Count == 0)
            {
                returnObj[2] = new ErrorDetails("Properties for the objects are not defined");
                return returnObj;
            }

            #endregion

            #region Build Field Names, Field Values and Parameters

            StringBuilder fieldNames = new StringBuilder();
            StringBuilder fieldValues = new StringBuilder();

            ArrayList paramList = new ArrayList();

            foreach (KeyValuePair<DBField, PropertyInfo> pair in propList)
            {
                #region Get DB Field Details

                DBField dbField = pair.Key;

                string dbFieldName = dbField.Name;
                bool isIdentityField = dbField.Identity;
                DefaultValueType defaultValueType = dbField.DefaultValueType;
                object defaultValue = dbField.DefaultValue;

                #endregion

                #region Get Property Details

                PropertyInfo pi = pair.Value;
                string propertyName = pi.Name;

                #endregion

                #region Check whether it is a Identity Field

                if (isIdentityField)
                {
                    continue;
                }

                #endregion

                #region Build Field Name and Field Values

                //Get Values from Object
                object value = type.GetProperty(propertyName).GetValue(obj, null);

                if (propertyName != DALConstants.LAST_UPDATED)
                {
                    if (value != null)
                    {
                        if (value.GetType() == typeof(DateTime))
                        {
                            DateTime date = Convert.ToDateTime(value);


                            if (date == DateTime.MinValue ||
                                            date == Convert.ToDateTime(Constants.DEFAULT_DATE_TIME_SQL))
                            {
                                value = DBNull.Value;
                            }
                        }
                    }
                }

                if (fieldNames.ToString() != string.Empty)
                {
                    fieldNames.Append(",");
                    fieldValues.Append(",");
                }

                if (pi.PropertyType == typeof(ReferenceDetails))
                {
                    object innerObj = pi.GetValue(obj, null);
                    PropertyInfo property = pi.GetValue(obj, null).GetType().GetProperty(DALConstants.ID);
                    value = property.GetValue(innerObj, null);
                }
                else if (pi.PropertyType.BaseType == typeof(Enum))
                {
                    value = Convert.ToInt16(value);
                }
                else if (defaultValue != null && defaultValueType == DefaultValueType.BuiltInValueType)
                {
                    value = defaultValue;
                }
                else if (defaultValue != null && defaultValueType == DefaultValueType.GetFromSessionDetails)
                {
                    Type sesDtlsType = sesDtls.GetType();
                    value = sesDtlsType.GetProperty(defaultValue.ToString()).GetValue(sesDtls, null);
                }

                fieldNames.Append(dbFieldName);
                if (defaultValueType == DefaultValueType.SQLFunction)
                {
                    fieldValues.Append(defaultValue);
                }
                else
                {
                    if (value == null)
                    {
                        value = DBNull.Value;
                    }
                    fieldValues.Append("@" + dbFieldName);

                    if (pi.PropertyType == typeof(System.Byte[]) &&
                            value == DBNull.Value)
                    {
                        SqlParameter parameter = new SqlParameter("@" + dbFieldName, SqlDbType.Image);
                        parameter.Value = value;
                        paramList.Add(parameter);
                    }
                    else
                    {
                        paramList.Add(new SqlParameter("@" + dbFieldName, value));
                    }
                }

                #endregion
            }

            #endregion

            #region Build Main Query

            StringBuilder sqlQry = new StringBuilder();
            sqlQry.Append("Insert into " + tableName + "(" + fieldNames.ToString() + ") values (");
            sqlQry.Append(fieldValues.ToString() + ")");

            #endregion

            #region Execute Query

            try
            {
                object idObj = ExecuteScalar(sqlQry.ToString() + ";SELECT Id = SCOPE_IDENTITY()", ConvertoArray(paramList));

                if (idObj == null)
                {
                    returnObj[0] = 0;
                    returnObj[1] = 0;
                    returnObj[2] = new ErrorDetails("Inserted Id is 0");
                }

                returnObj[0] = 1;
                returnObj[1] = idObj;
                returnObj[2] = null;
            }
            catch (Exception ex)
            {
                returnObj[0] = 0;
                returnObj[1] = 0;
                returnObj[2] = new ErrorDetails(ex.Message);
                return returnObj;
            }

            #endregion

            #region Return

            return returnObj;

            #endregion
        }

        #endregion

        #region Generic Update Method

        /// <summary>
        /// This method is used to Update the record in the table
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="tableName">Table Name</param>
        /// <param name="logEventId">Log EventId</param>
        /// <param name="type">Type of Object</param>
        /// <param name="propList">List of Properties</param>
        /// <returns>Error Details</returns>
        private ErrorDetails UpdateRecord(string tableName,
                                        object obj,
                                        Type type,
                                        Dictionary<DBField, PropertyInfo> propList,
                                        Dictionary<string, object> whereCondList)
        {

            #region Null check for Parameters

            if (obj == null || string.IsNullOrEmpty(tableName))
            {
                return new ErrorDetails("Table Name/Object is Null or Empty");
            }

            if (propList == null || propList.Count == 0)
            {
                return new ErrorDetails("Properties for the objects are not defined");
            }

            #endregion

            #region Build Set Field Names, Field Values and Parameters

            StringBuilder setFieldNames = new StringBuilder();

            ArrayList paramList = new ArrayList();

            foreach (KeyValuePair<DBField, PropertyInfo> pair in propList)
            {
                #region Get DBField Details

                DBField dbField = pair.Key;
                string dbFieldName = dbField.Name;
                bool isIdentityField = dbField.Identity;
                DefaultValueType defaultValueType = dbField.DefaultValueType;
                object defaultValue = dbField.DefaultValue;

                #endregion

                #region Get Property Details

                PropertyInfo pi = pair.Value;
                string propertyName = pi.Name;

                #endregion

                #region Check for Identity Column

                if (isIdentityField)
                {
                    continue;
                }

                #endregion

                #region Build Set Field SQL Query string

                object value = type.GetProperty(propertyName).GetValue(obj, null);

                if (propertyName != DALConstants.LAST_UPDATED)
                {
                    if (value != null)
                    {
                        if (value.GetType() == typeof(DateTime))
                        {
                            DateTime date = Convert.ToDateTime(value);

                            if (date == DateTime.MinValue ||
                                            date == Convert.ToDateTime(Constants.DEFAULT_DATE_TIME_SQL))
                            {
                                value = DBNull.Value;
                            }
                        }
                    }
                }

                if (setFieldNames.ToString() != string.Empty)
                {
                    setFieldNames.Append(",");
                }

                if (pi.PropertyType == typeof(ReferenceDetails))
                {
                    object innerObj = pi.GetValue(obj, null);
                    PropertyInfo property = pi.GetValue(obj, null).GetType().GetProperty("Id");
                    value = property.GetValue(innerObj, null);
                }
                else if (pi.PropertyType.BaseType == typeof(Enum))
                {
                    value = Convert.ToInt16(value);
                }
                else if (defaultValue != null && defaultValueType == DefaultValueType.BuiltInValueType)
                {
                    value = defaultValue;
                }
                else if (defaultValue != null && defaultValueType == DefaultValueType.GetFromSessionDetails)
                {
                    Type sesDtlsType = sesDtls.GetType();
                    value = sesDtlsType.GetProperty(defaultValue.ToString()).GetValue(sesDtls, null);
                }

                if (defaultValueType == DefaultValueType.SQLFunction)
                {
                    setFieldNames.Append(dbFieldName + " = " + defaultValue);
                }
                else
                {
                    if (value == null)
                    {
                        value = DBNull.Value;
                    }

                    setFieldNames.Append(dbFieldName + " = @set" + dbFieldName);

                    if (pi.PropertyType == typeof(System.Byte[]) &&
                            value == DBNull.Value)
                    {
                        SqlParameter parameter = new SqlParameter("@set" + dbFieldName, SqlDbType.Image);
                        parameter.Value = value;
                        paramList.Add(parameter);
                    }
                    else
                    {
                        paramList.Add(new SqlParameter("@set" + dbFieldName, value));
                    }
                }

                #endregion
            }

            #endregion

            #region Build Where condition

            StringBuilder whereConditions = new StringBuilder();

            foreach (KeyValuePair<string, object> whereCond in whereCondList)
            {
                if (whereConditions.ToString() != string.Empty)
                {
                    whereConditions.Append(" and ");
                }

                string whereConditionField = whereCond.Key;
                object whereConsitionValue = whereCond.Value;

                whereConditions.Append(whereConditionField + " = @where" + whereConditionField);
                paramList.Add(new SqlParameter("@where" + whereConditionField, whereConsitionValue));
            }

            #endregion

            #region Build Main Query

            StringBuilder sqlQry = new StringBuilder();
            sqlQry.Append("Update " + tableName + " Set " + setFieldNames.ToString() + " Where " + whereConditions.ToString());

            #endregion

            #region Execute Query

            try
            {
                ExecuteNonQuery(sqlQry.ToString(), ConvertoArray(paramList));
            }
            catch (Exception ex)
            {
                return new ErrorDetails(ex.Message);
            }

            #endregion

            #region Return

            return null;

            #endregion
        }

        /// <summary>
        /// This method is used to update the record in the table based on the record Id
        /// </summary>
        /// <param name="obj">Object</param>a        
        /// <param name="lovEventId">Log Event Id</param>
        /// <returns>Error Details</returns>
        public ErrorDetails UpdateRecord(object obj)
        {
            #region Get Type

            Type type = obj.GetType();

            #endregion

            #region Get Properties

            Dictionary<DBField, PropertyInfo> propList = GetObjectProperties(type, obj);

            #endregion

            #region Get Table Name

            string tableName = GetTableName(obj);

            #endregion

            #region Build Where Condition

            object valueId = type.GetProperty(DALConstants.ID).GetValue(obj, null);

            Dictionary<string, object> whereConds = new Dictionary<string, object>();
            whereConds[DALConstants.ID] = valueId;

            #endregion

            #region Upate Record

            return UpdateRecord(tableName, obj, type, propList, whereConds);

            #endregion
        }

        #endregion

        #region Generic Fetch Method

        /// <summary>
        /// This method is used to Fetch the record by query
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="sqlQry">Query string</param>
        /// <returns>List of objects</returns>
        public List<T> FetchRecord<T>(string sqlQry) where T : new()
        {
            #region Null check for Parameters

            if (string.IsNullOrEmpty(sqlQry))
            {
                return null;
            }

            #endregion

            #region Execute Query and Return

            IDataReader reader = ExecuteDataReader(sqlQry);

            return ConvertDataReaderToObject<T>(reader);

            #endregion
        }

        /// <summary>
        /// This method is used to Fetch All records from the table
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns>List of Objects</returns>
        public List<T> FetchAllRecords<T>(Dictionary<string, object> wherCondition) where T : new()
        {
            return FetchRecord<T>(null, wherCondition);
        }

        /// <summary>
        /// This method is used to Fetch the Record from the table
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="fieldNameList">Field List if it is null the all fields will be fetched</param>
        /// <param name="wherCondition">Where condition if it is null then all records will be fetched</param>
        /// <returns>List of Objects</returns>
        public List<T> FetchRecord<T>(List<string> fieldNameList,
                                        Dictionary<string, object> wherCondition) where T : new()
        {
            #region Build Field Name string

            StringBuilder fieldNames = new StringBuilder();

            if (fieldNameList == null || fieldNameList.Count == 0)
            {
                fieldNames.Append(" * ");
            }
            else
            {
                foreach (string fieldName in fieldNameList)
                {
                    if (fieldNames.ToString() != string.Empty)
                    {
                        fieldNames.Append(", ");
                    }

                    fieldNames.Append(fieldName);
                }
            }

            #endregion

            #region Build Where Condition string

            StringBuilder whereConds = new StringBuilder();
            ArrayList paramList = new ArrayList();
            if (wherCondition == null)
            {
                whereConds.Append(string.Empty);
            }
            else
            {
                foreach (KeyValuePair<string, object> whereCond in wherCondition)
                {
                    string fieldName = whereCond.Key;
                    object fieldValue = whereCond.Value;

                    if (whereConds.ToString() != string.Empty)
                    {
                        whereConds.Append(" and ");
                    }

                    whereConds.Append(fieldName + " = @" + fieldName);

                    paramList.Add(new SqlParameter("@" + fieldName, fieldValue));
                }
            }

            #endregion

            #region Get Table Name

            string tableName = GetTableName<T>();

            #endregion

            #region Build Main SQL Query string

            StringBuilder sqlQry = new StringBuilder();
            sqlQry.Append("Select " + fieldNames + " from " + tableName);

            if (wherCondition != null)
            {
                sqlQry.Append(" where " + whereConds);
            }

            #endregion

            #region Execute Query

            IDataReader reader = ExecuteDataReader(sqlQry.ToString(), ConvertoArray(paramList));

            #endregion

            #region Fill and Return

            return ConvertDataReaderToObject<T>(reader);

            #endregion
        }

        /// <summary>
        /// This method is used to Fetch the Record from the table
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="fieldNameList">Field List if it is null the all fields will be fetched</param>
        /// <param name="wherCondition">Where condition if it is null then all records will be fetched</param>
        /// <returns>List of Objects</returns>
        public T FetchSingleRecord<T>(List<string> fieldNameList,
                                        Dictionary<string, object> wherCondition) where T : new()
        {
            List<T> objList = FetchRecord<T>(fieldNameList, wherCondition);
            if (objList != null &&
                   objList.Count > 0)
            {
                return objList[0];
            }
            return default(T);
        }

        #endregion

        #region Support Private Methods For Generic DALS

        /// <summary>
        /// This method is used get the Table Name based on the Type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns>Table Name</returns>
        private string GetTableName<T>() where T : new()
        {
            T obj = new T();
            return GetTableName(obj);
        }

        /// <summary>
        /// This method is used to Get the Table name based on the object
        /// </summary>
        /// <param name="obj">Object</param>
        /// <returns>Table Name</returns>
        public string GetTableName(object obj)
        {
            Type type = obj.GetType();
            return GetTableName(type);
        }

        /// <summary>
        /// This method is used to Get the Table Name based on the Type
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Table Name</returns>
        private string GetTableName(Type type)
        {
            #region Read Attribute and Get Table Name

            string tableName = string.Empty;

            object[] tableNames = type.GetCustomAttributes(typeof(DBTableName), true);
            foreach (DBTableName tblName in tableNames)
            {
                tableName = tblName.FieldName;
            }

            #endregion

            #region Return

            return tableName;

            #endregion
        }

        /// <summary>
        /// This method is used to Get the Object Properties
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="obj">Object</param>
        /// <returns>List of Properties</returns>
        private Dictionary<DBField, PropertyInfo> GetObjectProperties(Type type,
                                                                        object obj)
        {
            #region Null check for Parameters

            if (obj == null)
            {
                return null;
            }

            #endregion

            #region Get Property List

            Dictionary<DBField, PropertyInfo> propList = new Dictionary<DBField, PropertyInfo>();

            foreach (PropertyInfo propInfo in type.GetProperties())
            {
                foreach (DBField dbField in propInfo.GetCustomAttributes(typeof(DBField), true))
                {
                    propList[dbField] = propInfo;
                }
            }

            #endregion

            #region Return

            return propList;

            #endregion
        }

        #endregion

    }
}
