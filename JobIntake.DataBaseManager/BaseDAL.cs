﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using JobIntake.DataObject;
using System.Collections;
using System.Configuration;
using System.Reflection;

namespace JobIntake.DataBaseManager
{
    public class BaseDAL
    {
        #region Private Members

        /// <summary>
        /// isOwner is True, if DAL Owns the transaction
        /// </summary>
        private bool isOwner = false;
        /// <summary>
        /// Reference to the current Transaction
        /// </summary>
        private SqlTransaction sqlTxn;
        /// <summary>
        /// Session Details will be set by derived class
        /// </summary>
        private SessionDetails SesDtls;

        private static int sqlCommandTimeOutSecondsValue = 200;
        private const string COMPANY_ID = "CompanyId";
        private const string PARAM_COMPANY_ID = "@CompanyId";
        private const string SQLCOMMANDTIMEOUTSECONDS = "SqlCommandTimeoutSeconds";

        #endregion

        #region Properties

        public SqlTransaction SqlTxn
        {
            get { return sqlTxn; }
            set { sqlTxn = value; }
        }

        public SessionDetails sesDtls
        {
            get { return SesDtls; }
            set { SesDtls = value; }
        }

        #endregion

        #region Constructor

        public BaseDAL(IDbTransaction sqlTxn)
        {
            if (sqlTxn == null)
            {
                this.isOwner = true;
            }
            else
            {
                this.sqlTxn = (SqlTransaction)sqlTxn;
                this.isOwner = false;
            }
        }

        static BaseDAL()
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(SQLCOMMANDTIMEOUTSECONDS) &&
                !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[SQLCOMMANDTIMEOUTSECONDS]))
            {
                sqlCommandTimeOutSecondsValue = Convert.ToInt32(ConfigurationManager.AppSettings[SQLCOMMANDTIMEOUTSECONDS]);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This Method is used to conver the ParameterArray into the SQLParam List
        /// </summary>
        /// <param name="paramList">Parameter Array</param>
        /// <returns>SqlParameter Array</returns>
        private SqlParameter[] ConvertoArray(ArrayList paramList)
        {
            SqlParameter[] paramArray = new SqlParameter[paramList.Count];
            paramArray = (SqlParameter[])paramList.ToArray(typeof(SqlParameter));
            return paramArray;
        }

        /// <summary>
        /// Overload Method for Creating a Sql Command Object
        /// </summary>
        /// <param name="sqlQry">Query String</param>
        /// <returns>Sql Command Object</returns>
        private SqlCommand CreateSqlCommand(string sqlQry)
        {
            SqlCommand sqlCmd = new SqlCommand(sqlQry);

            return sqlCmd;
        }

        /// <summary>
        /// Overload Method for Creating a Sql Command Object
        /// </summary>
        /// <param name="sqlQry">Query String</param>
        /// <param name="parameterArray">Arrary of Sql Parameters</param>
        /// <returnsSql Command Object></returns>
        private SqlCommand CreateSqlCommand(string sqlQry, SqlParameter[] parameterArray)
        {
            SqlCommand sqlCmd = new SqlCommand(sqlQry);
            sqlCmd = BuildParameters(sqlCmd, parameterArray);

            return sqlCmd;
        }

        /// <summary>
        /// Overload Method for Creating a Sql Command Object
        /// </summary>
        /// <param name="dataObject">Data Object</param>
        /// <param name="sqlQry">Query String</param>
        /// <param name="placeHolderArray">Array of Place Holders</param>
        /// <returns>Sql Command Object</returns>
        private SqlCommand CreateSqlCommand(object dataObject, string sqlQry, string[] placeHolderArray)
        {
            SqlCommand sqlCmd = new SqlCommand(sqlQry);
            sqlCmd = BuildParameters(dataObject, sqlCmd, placeHolderArray);

            return sqlCmd;
        }

        /// <summary>
        /// Private method to Prepare the SQL Command and returns Sql Command with the proper connection
        /// and Proper Transaction
        /// </summary>
        /// <param name="sqlQry"></param>
        /// <returns></returns>
        private SqlCommand PrepareSqlCommand(SqlCommand sqlCmd)
        {
            if (isOwner)
            {
                SqlConnection sqlConn = TransactionManager.GetConnection(sesDtls);
                sqlConn.Open();
                SqlTransaction sqlTempTxn = sqlConn.BeginTransaction(IsolationLevel.ReadCommitted);
                sqlCmd.Connection = sqlConn;
                sqlCmd.Transaction = sqlTempTxn;

                //This check is to avoid overriding timeout, if somewhere it is set to 0 deliberately
                if (sqlCmd.CommandTimeout != 0)
                {
                    sqlCmd.CommandTimeout = sqlCommandTimeOutSecondsValue;
                }
            }
            else
            {
                sqlCmd.Connection = this.sqlTxn.Connection;
                sqlCmd.Transaction = this.sqlTxn;
            }
            return sqlCmd;
        }

        /// <summary>
        /// This method is used to Build the SQL Parameters based on the Sql Parameter Array
        /// </summary>
        /// <param name="sqlCommand">Command Object</param>
        /// <param name="parameterArray">Parameter Array</param>
        private SqlCommand BuildParameters(SqlCommand sqlCmd, SqlParameter[] parameterArray)
        {
            sqlCmd.Parameters.AddRange(parameterArray);
            return sqlCmd;
        }

        /// <summary>
        /// This method is used to Build the SQL Parameters based on the Place Holders Array
        /// </summary>
        /// <param name="dataObject">Data Object</param>
        /// <param name="sqlCommand">Command Object</param>
        /// <param name="placeHoldersArray">Parameter Array</param>
        private SqlCommand BuildParameters(object dataObject, SqlCommand sqlCmd, string[] placeHoldersArray)
        {
            foreach (string placeHolder in placeHoldersArray)
            {
                string name = placeHolder.Replace('@', ' ').Trim();
                if (name.Contains("_"))
                {
                    string tempString = "@" + name;
                    string[] refArray = name.Split('_');
                    PropertyInfo propertyRefDetails = dataObject.GetType().GetProperty(refArray[0]);
                    object objectRef = propertyRefDetails.GetValue(dataObject, null);
                    PropertyInfo propertyRef = objectRef.GetType().GetProperty(refArray[1]);
                    object propertyRefValue = propertyRef.GetValue(objectRef, null);
                    SqlParameter sqlParam = new SqlParameter(tempString, propertyRefValue);
                    sqlCmd.Parameters.Add(sqlParam);
                }
                //else if (name == COMPANY_ID)
                //{
                //    switch (companyType)
                //    {
                //        case Company.DEPENDENT:
                //            SqlParameter sqlParamCompany = new SqlParameter(PARAM_COMPANY_ID, sesDtls.CompanyId);
                //            sqlCmd.Parameters.Add(sqlParamCompany);
                //            break;
                //    }
                //}
                else
                {
                    PropertyInfo property = dataObject.GetType().GetProperty(name);
                    object value = property.GetValue(dataObject, null);
                    name = "@" + name;
                    SqlParameter sqlParameter = null;
                    if (value == null)
                    {
                        sqlParameter = new SqlParameter(name, DBNull.Value);
                    }
                    else
                    {
                        sqlParameter = new SqlParameter(name, value);
                    }
                    sqlCmd.Parameters.Add(sqlParameter);
                }
            }

            return sqlCmd;
        }

        /// <summary>
        /// Executes a command object according to the return type.
        /// </summary>
        /// <param name="RetType">Return type</param>
        /// <param name="MissingSchemaAction">Specifies the action to take when adding data to the DataSet and the required DataTable or DataColumn is missing</param>
        private object ExecuteCommand(SqlCommand sqlCmd,
                                        Type retType,
                                        MissingSchemaAction missingSchemaAction)
        {
            try
            {
                object result = null;

                if (sqlCmd.Connection.State == ConnectionState.Closed)
                {
                    sqlCmd.Connection.Open();
                }

                if (retType.FullName == "System.Void")
                {
                    object obj = sqlCmd.ExecuteNonQuery();
                    result = obj;
                }
                else if (retType == typeof(DataSet))
                {
                    DataSet sqlDS = new DataSet();
                    SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
                    sqlDA.MissingSchemaAction = missingSchemaAction;
                    sqlDA.Fill(sqlDS);
                    result = sqlDS;
                }
                else if (retType == typeof(DataTable))
                {
                    DataTable sqlDT = new DataTable();
                    SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
                    sqlDA.MissingSchemaAction = missingSchemaAction;
                    sqlDA.Fill(sqlDT);
                    result = sqlDT;
                }
                else if (retType == typeof(SqlDataReader))
                {
                    SqlDataReader sqlDR = null;

                    //Added the CloseConnection Command Behaviour by Gokul
                    //This is required to close the connection when the data
                    //reader is closed.
                    sqlDR = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
                    result = sqlDR;
                }
                else if (retType == typeof(SqlDataAdapter))
                {
                    SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
                    sqlDA.MissingSchemaAction = missingSchemaAction;
                    result = sqlDA;
                }
                else if (retType == typeof(SqlCommand))
                {
                    result = sqlCmd;
                }
                else
                {
                    object obj = sqlCmd.ExecuteScalar();
                    result = obj;
                }
                if ((retType != typeof(SqlDataReader)) && (isOwner))
                {
                    sqlCmd.Transaction.Commit();
                    sqlCmd.Connection.Close();
                    if (sqlCmd != null)
                    {
                        sqlCmd.Dispose();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (isOwner && sqlCmd != null && sqlCmd.Connection != null && sqlCmd.Connection.State != ConnectionState.Closed)
                {
                    if (sqlCmd.Transaction != null)
                    {
                        sqlCmd.Transaction.Rollback();
                    }
                    sqlCmd.Connection.Close();
                    sqlCmd.Dispose();
                }

                throw;
            }
        }


        #endregion

        #region Internal Methods
        /// <summary>
        /// Sets the MissingSchemaAction = Add then calls the overloaded ExecuteCommand method which has four parameters
        /// </summary>
        /// <remarks>MissingSchemaAction  - Specifies the action to take when adding data to the DataSet and the required DataTable or DataColumn is missing</remarks>
        internal object ExecuteCommand(SqlCommand sqlCmd,
                                        Type retType)
        {
            return ExecuteCommand(sqlCmd, retType, MissingSchemaAction.Add);
        }
        #endregion

        #region Execute Methods using Query

        /// <summary>
        /// Executes SqlQry string and returns DataSet object.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        public DataSet ExecuteDataset(string sqlQry)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            return ExecuteDataset(sqlCmd);
        }

        /// <summary>
        /// Executes the Query and returns the Data Reader
        /// </summary>
        /// <param name="sqlQry">Query String</param>
        /// <returns>Data reader</returns>
        public SqlDataReader ExecuteDataReader(string sqlQry)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            return ExecuteDataReader(sqlCmd);
        }

        /// <summary>
        /// Executes SqlQry string and returns DataTable object.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        public DataTable ExecuteDataTable(string sqlQry)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            return ExecuteDataTable(sqlCmd);
        }

        /// <summary>
        /// Executes SqlQry string and returns no result.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        /// <returns>Number of records affected</returns>
        public int ExecuteNonQuery(string sqlQry)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            return ExecuteNonQuery(sqlCmd);
        }

        /// <summary>
        /// Executes SqlQry string and returns scalar value.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        public object ExecuteScalar(string sqlQry)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            return ExecuteScalar(sqlCmd);
        }

        #endregion

        #region Execute Methods using Query and Command Type

        /// <summary>
        /// Executes SqlQry string and returns DataSet object.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        public DataSet ExecuteDataset(string sqlQry,
                                        CommandType commandType)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry);
            sqlCmd.CommandType = commandType;
            return ExecuteDataset(sqlCmd);
        }

        #endregion

        #region Execute Methods using Query and Param Array

        /// <summary>
        /// Exeuctes the datareader based on the Query string and the Parameter Array
        /// </summary>
        /// <param name="sqlQry">Query string</param>
        /// <param name="parameterArray">Array of Sql Parameters</param>
        /// <returns>Data Reader</returns>
        public SqlDataReader ExecuteDataReader(string sqlQry,
                                                SqlParameter[] parameterArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry, parameterArray);
            return ExecuteDataReader(sqlCmd);
        }

        /// <summary>
        /// Executes SqlQry string and returns no result
        /// </summary>
        /// <param name="sqlQry">>Query string</param>
        /// <param name="sqlParamArray">SQL Param Array</param>
        /// <returns>Number of records affected</returns>
        public int ExecuteNonQuery(string sqlQry,
                                    SqlParameter[] sqlParamArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry, sqlParamArray);
            object obj = ExecuteNonQuery(sqlCmd);
            sqlCmd.Parameters.Clear();
            if (obj != null && obj != DBNull.Value)
            {
                return (int)obj;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Executes SqlQry string and returns DataTable object.
        /// </summary>
        /// <param name="sqlQry">Query string</param>
        /// <param name="sqlParamArray">SQL Parameter List</param>
        /// <returns>Data Table</returns>
        public DataTable ExecuteDataTable(string sqlQry,
                                            SqlParameter[] sqlParamArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry, sqlParamArray);
            object obj = ExecuteDataTable(sqlCmd);
            sqlCmd.Parameters.Clear();
            if (obj != null && obj != DBNull.Value)
            {
                return (DataTable)obj;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Executes SqlQry string and returns scalar value.
        /// </summary>
        /// <param name="sqlParamArray">SQL Parameter Array</param>
        /// <param name="sqlQry">Query String</param>
        public object ExecuteScalar(string sqlQry,
                                        SqlParameter[] sqlParamArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(sqlQry, sqlParamArray);
            return ExecuteScalar(sqlCmd);
        }

        #endregion

        #region Execute Methods using Query and Param List

        /// <summary>
        /// Executes SqlQry string and returns no result.
        /// </summary>
        /// <param name="sqlQry">Query string</param>
        /// <param name="paramsList">params array List</param>
        /// <returns>Number of records affected</returns>
        public int ExecuteNonQuery(string sqlQry,
                                    ArrayList paramsList)
        {

            return ExecuteNonQuery(sqlQry, ConvertoArray(paramsList));
        }

        /// <summary>
        /// Execute Scalar
        /// </summary>
        /// <param name="sqlQry">SQL Query</param>
        /// <param name="paramsList">Param List</param>
        /// <returns>Object</returns>
        public object ExecuteScalar(string sqlQry,
                                        ArrayList paramsList)
        {
            return ExecuteScalar(sqlQry, ConvertoArray(paramsList));
        }

        #endregion

        #region Execute Methods using Sql Command

        /// <summary>
        /// Executes the Sql Command Object and returns Dataset
        /// </summary>
        /// <param name="sqlCmd">Sql Command Object</param>
        /// <returns>Dataset</returns>
        public DataSet ExecuteDataset(SqlCommand sqlCmd)
        {
            sqlCmd = PrepareSqlCommand(sqlCmd);
            return (DataSet)ExecuteCommand(sqlCmd, typeof(DataSet));
        }

        /// <summary>
        /// Executes the sql command and returns the data reader
        /// </summary>
        /// <param name="sqlCmd">Sql Command</param>
        /// <returns>Data Reader</returns>
        public SqlDataReader ExecuteDataReader(SqlCommand sqlCmd)
        {
            sqlCmd = PrepareSqlCommand(sqlCmd);
            return (SqlDataReader)ExecuteCommand(sqlCmd, typeof(SqlDataReader));
        }

        /// <summary>
        /// Executes the sql command and returns the data table
        /// </summary>
        /// <param name="sqlCmd">Sql Command</param>
        /// <returns>Data table</returns>
        public DataTable ExecuteDataTable(SqlCommand sqlCmd)
        {
            sqlCmd = PrepareSqlCommand(sqlCmd);
            return (DataTable)ExecuteCommand(sqlCmd, typeof(DataTable));
        }

        /// <summary>
        /// Executes the Sql Command Object and returns the Number of rows affected
        /// </summary>
        /// <param name="sqlCmd">Sql Command</param>
        /// <returns>Number of rows afffected</returns>
        public int ExecuteNonQuery(SqlCommand sqlCmd)
        {
            sqlCmd = PrepareSqlCommand(sqlCmd);
            return (int)ExecuteCommand(sqlCmd, typeof(void));
        }

        /// <summary>
        /// Executes the Sql command and returns the Object
        /// </summary>
        /// <param name="sqlCmd">Command Object</param>
        /// <returns>First Row and First column of the result set</returns>
        public object ExecuteScalar(SqlCommand sqlCmd)
        {
            sqlCmd = PrepareSqlCommand(sqlCmd);
            return ExecuteCommand(sqlCmd, typeof(object));
        }

        #endregion

        #region Execute Methods by Data Object

        /// <summary>
        /// Executes the Data reader using the Query string and gets the parameter values from the data object
        /// </summary>
        /// <param name="dataObject">Data Object to Fetch the Parameter values</param>
        /// <param name="sqlQry">Query String</param>
        /// <param name="placeHolderArray">Array of Place Holders</param>
        /// <param name="companyType">CompanyType-Dependent/Independent/Shared </param> 
        /// <returns>Data reader</returns>
        public SqlDataReader ExecuteDataReader(object dataObject,
                                                    string sqlQry,
                                                    string[] placeHolderArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(dataObject, sqlQry, placeHolderArray);
            return ExecuteDataReader(sqlCmd);
        }

        /// <summary>
        /// Executes SqlQry string and returns DataTable object.
        /// </summary>
        /// <param name="SqlQry">Query String</param>
        public DataTable ExecuteDataTable(object dataObject,
                                            string sqlQry,
                                            string[] placeHolderArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(dataObject, sqlQry, placeHolderArray);
            return ExecuteDataTable(sqlCmd);
        }

        /// <summary>
        /// Overload method to Executes SqlQry string and returns the number of rows affected.
        /// </summary>
        /// <param name="dataObject">Data Object</param>
        /// <param name="SqlQry">Query String</param>
        /// <param name="placeHolderArray">Parameter Array</param>
        /// <returns>Number of records affected</returns>
        public int ExecuteNonQuery(object dataObject,
                                    string sqlQry,
                                    string[] placeHolderArray)
        {
            SqlCommand sqlCmd = CreateSqlCommand(dataObject, sqlQry, placeHolderArray);
            return ExecuteNonQuery(sqlCmd);
        }

        #endregion

        #region Other Public Methods

        /// <summary>
        /// Get Stored Procedure Parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored Procedure Name</param>
        /// <returns>Parameteters</returns>
        public SqlParameterCollection GetStoredProcedureParameters(string storedProcedureName)
        {
            SqlCommand sqlCmd = CreateSqlCommand(storedProcedureName);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd = PrepareSqlCommand(sqlCmd);

            SqlCommandBuilder.DeriveParameters(sqlCmd);

            return sqlCmd.Parameters;
        }

        /// <summary>
        /// Batch Insert
        /// </summary>
        /// <param name="tableName">Destination Table Name</param>
        /// <param name="dt">Data Table</param>
        /// <returns>Error Details</returns>
        public ErrorDetails BatchInsert(string tableName,
                                            DataTable dt)
        {
            #region Null check for Parameters

            if (string.IsNullOrEmpty(tableName))
            {
                return new ErrorDetails("In Batch Insert: Destination Table Name is Empty");
            }

            if (dt == null)
            {
                return null;
            }

            #endregion

            #region Local Variables

            SqlBulkCopy sbc = null;

            #endregion

            try
            {
                #region Create SQL Bulk Copy Object

                if (isOwner)
                {
                    SqlConnection sqlConn = TransactionManager.GetConnection(sesDtls);
                    sbc = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.UseInternalTransaction, null);
                }
                else
                {
                    sbc = new SqlBulkCopy(sqlTxn.Connection, SqlBulkCopyOptions.Default, sqlTxn);
                }

                #endregion

                #region Assign Destination Table, Batch Size and Column Mappings

                sbc.DestinationTableName = tableName;
                sbc.BatchSize = 2;

                foreach (DataColumn dc in dt.Columns)
                {
                    sbc.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                }

                #endregion

                #region Execute Batch Insert

                sbc.WriteToServer(dt);

                #endregion
            }
            catch (Exception ex)
            {
                #region Close SBC

                if (sbc != null)
                {
                    sbc.Close();
                }

                return new ErrorDetails(ex.Message);

                #endregion
            }
            finally
            {
                #region Close SBC

                if (sbc != null)
                {
                    sbc.Close();
                }

                #endregion
            }

            #region Return

            return null;

            #endregion
        }

        #endregion
    }
}
