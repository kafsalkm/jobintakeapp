﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JobIntake.DataObject;
using JobIntakeBI;

namespace JobIntake.Api
{

    public class HomeController : ApiController
    {


        #region Intialize Variables

        SessionDetails _sessionDetails = new SessionDetails();

        #endregion

        #region Constructor

        public HomeController()
        {
            _sessionDetails.DBName = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        #endregion

        /// <summary>
        /// API to Job details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpGet]
        [Route("api/Home/GetJobById")]
        public ObjectResult GetJobById(long JobId)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            #endregion

            #region  Parameter Null Check

            if (JobId == null || JobId == 0)
            {
                _objRes.message = "Parameter JobId is Missing";
                _objRes.status = 0;
                _objRes.results = null;
            }

            #endregion

            #region Fetch Job

            try
            {
                Job objJob = iJobMgt.FetchJobByJobId(_sessionDetails, JobId);
                if (objJob != null)
                {
                    _objRes.results = objJob;
                    _objRes.status = 1;
                    _objRes.message = "Job Details fetched successfully";
                }
                else
                {
                    _objRes.message = "No Job is found with the given JobId";
                    _objRes.status = 0;
                    _objRes.results = null;
                }
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion

        }

        /// <summary>
        /// API to fetch Job details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpGet]
        [Route("api/Home/GetJobsByClientId")]
        public ObjectResult GetJobsByClientId(long ClientId)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            #endregion

            #region  Parameter Null Check

            if (ClientId == null || ClientId == 0)
            {
                _objRes.message = "Parameter ClientId is Missing";
                _objRes.status = 0;
                _objRes.results = null;
            }

            #endregion

            #region Fetch JobId

            try
            {
                List<Job> lstJob = iJobMgt.FetchJobsByClientId(_sessionDetails, ClientId);
                if (lstJob != null)
                {
                    _objRes.results = lstJob;
                    _objRes.status = 1;
                    _objRes.message = "Job Details fetched successfully";
                }
                else
                {
                    _objRes.message = "No Job is found with the given Client";
                    _objRes.status = 0;
                    _objRes.results = null;
                }
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion

        }

        /// <summary>
        /// API to fetch all codes
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpGet]
        [Route("api/Home/FetchAllCodes")]
        public ObjectResult FetchAllCodes()
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            #endregion

            #region  Parameter Null Check


            #endregion

            #region Fetch JobId

            try
            {
                List<GenericCode> objCodes= iJobMgt.FetchAllCodes(_sessionDetails);
                if (objCodes != null)
                {
                    _objRes.results = objCodes;
                    _objRes.status = 1;
                    _objRes.message = "Codes Details fetched successfully";
                }
                else
                {
                    _objRes.message = "No codes is found with the given Client";
                    _objRes.status = 0;
                    _objRes.results = null;
                }
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion

        }

        /// <summary>
        /// API to fetch client details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpGet]
        [Route("api/Home/FetchClientByClientId")]
        public ObjectResult FetchClientByClientId(long ClientId)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            #endregion

            #region  Parameter Null Check

            if (ClientId == null || ClientId == 0)
            {
                _objRes.message = "Parameter ClientId is missing";
                _objRes.status = 0;
                _objRes.results = null;

            }

            #endregion

            #region Fetch Client Details

            try
            {
                Client objClient = iJobMgt.FetchClientByClientId(_sessionDetails, ClientId);
                if (objClient != null)
                {
                    _objRes.results = objClient;
                    _objRes.status = 1;
                    _objRes.message = "Client Details fetched successfully";
                }
                else
                {
                    _objRes.message = "No client is found with the given Client";
                    _objRes.status = 0;
                    _objRes.results = null;
                }
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion

        }

        /// <summary>
        /// API to insert New Job details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpPost]
        [Route("api/Home/InsertJob")]
        public ObjectResult InsertJob([FromBody]dynamic parameters)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            Job objJob;
            #endregion

            #region  Parameter Null Check
            if (parameters == null)
            {
                _objRes.message = "Please enter Job Deatils";
                _objRes.status = 0;
                return _objRes;

            }
            #endregion

            #region Getting parameter

            JObject jwJob = parameters.Job;

            if (jwJob == null)
            {
                _objRes.message = "Please add Job Deatils";
                _objRes.status = 0;
                return _objRes;
            }
            string jobDet = jwJob.ToString(Newtonsoft.Json.Formatting.None);
            objJob = JsonConvert.DeserializeObject<Job>(jobDet);

            #endregion

            #region Insert Job Details

            try
            {
                ErrorDetails err = iJobMgt.InsertJob(_sessionDetails, objJob);
                if (err == null)
                {
                    _objRes.status = 1;
                    _objRes.message = "Successfully updated Job Details";
                }
                else
                {
                    _objRes.status = 0;
                    _objRes.message = err.ErrorMessage;
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion
        }

        /// <summary>
        /// API to isert new client details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpPost]
        [Route("api/Home/InsertClient")]
        public ObjectResult Insertlient([FromBody]dynamic parameters)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            Client objClient;
            #endregion

            #region  Parameter Null Check
            if (parameters == null)
            {
                _objRes.message = "Please enter Client Deatils";
                _objRes.status = 0;
                return _objRes;

            }
            #endregion

            #region Getting parameter

            JObject jwJob = parameters.Job;

            if (jwJob == null)
            {
                _objRes.message = "Please enter Client Deatils";
                _objRes.status = 0;
                return _objRes;
            }
            string clientDet = jwJob.ToString(Newtonsoft.Json.Formatting.None);
            objClient = JsonConvert.DeserializeObject<Client>(clientDet);

            #endregion

            #region Insert Client Details

            try
            {
                ErrorDetails err = iJobMgt.InsertClient(_sessionDetails, objClient);
                if (err == null)
                {
                    _objRes.status = 1;
                    _objRes.message = "Successfully updated Job Details";
                }
                else
                {
                    _objRes.status = 0;
                    _objRes.message = err.ErrorMessage;
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion
        }

        /// <summary>
        /// API to update job details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpPost]
        [Route("api/Home/UpdateJob")]
        public ObjectResult UpdateJob([FromBody]dynamic parameters)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            Job objJob;
            #endregion

            #region  Parameter Null Check
            if (parameters == null)
            {
                _objRes.message = "Job Deatils is null";
                _objRes.status = 0;
                return _objRes;

            }
            #endregion

            #region Getting parameter

            JObject jwJob = parameters.Job;

            if (jwJob == null)
            {
                _objRes.message = "Please enter Job Deatils";
                _objRes.status = 0;
                return _objRes;
            }
            string jobDet = jwJob.ToString(Newtonsoft.Json.Formatting.None);
            objJob = JsonConvert.DeserializeObject<Job>(jobDet);

            #endregion

            #region Update Job Details

            try
            {
                ErrorDetails err = iJobMgt.UpdateJobDetails(_sessionDetails, objJob);
                if (err == null)
                {
                    _objRes.status = 1;
                    _objRes.message = "Successfully updated Job Details";
                }
                else
                {
                    _objRes.status = 0;
                    _objRes.message = err.ErrorMessage;
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion
        }
    }
}
