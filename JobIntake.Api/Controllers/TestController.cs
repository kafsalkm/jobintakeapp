﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JobIntake.DataObject;
using JobIntakeBI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace JobIntake.Api.Controllers
{
    public class TestController : ApiController
    {

        #region Intialize Variables

        SessionDetails _sessionDetails = new SessionDetails();

        #endregion

        #region Constructor

        public TestController()
        {
            _sessionDetails.DBName = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        #endregion

        /// <summary>
        /// API to insert New Job details
        /// </summary>
        /// <returns>ObjectResult in JSON</returns>
        [HttpPost]
        [Route("Api/Test/InsertJob")]
        public ObjectResult InsertJob([FromBody]dynamic parameters)
        {
            #region  Intialize Parameters
            ObjectResult _objRes = new ObjectResult();
            IJobIntakeMgt iJobMgt = new JobIntakeMgt();
            Job objJob;
            #endregion

            #region  Parameter Null Check
            if (parameters == null)
            {
                _objRes.message = "Please enter Job Deatils";
                _objRes.status = 0;
                return _objRes;

            }
            #endregion

            #region Getting parameter

            JObject jwJob = parameters.Job;

            if (jwJob == null)
            {
                _objRes.message = "Please add Job Deatils";
                _objRes.status = 0;
                return _objRes;
            }
            string jobDet = jwJob.ToString(Newtonsoft.Json.Formatting.None);
            objJob = JsonConvert.DeserializeObject<Job>(jobDet);

            #endregion

            #region Fetch Client Details

            try
            {
                ErrorDetails err = iJobMgt.InsertJob(_sessionDetails, objJob);

            }
            catch (Exception ex)
            {
                _objRes.status = 0;
                _objRes.message = "Something went wrong: " + ex.Message;
            }

            return _objRes;
            #endregion
        }
    }
}
