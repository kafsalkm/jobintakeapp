﻿using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.Cors;
using Newtonsoft.Json;

namespace JobIntake.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.MapHttpAttributeRoutes();
            //config.EnableCors(new EnableCorsAttribute("*", "Origin, Content-Type, Accept",
            //                                 "GET, PUT, POST, DELETE, OPTIONS"));
            //config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var json = config.Formatters.JsonFormatter;
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings();


            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Home", id = RouteParameter.Optional }
             );
        }
    }
}
