﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Controller to handle Job detail page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("JobDetailCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash', 'localStorageService', 'commonService', '$linq', '$mdDialog',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, localStorageService, commonService, $linq, $mdDialog) {
    
    $scope.loadPanelVisible = false;
    $scope.Skills = [];
    $scope.JobId = localStorageService.get("jobId");
    $scope.Codes = localStorageService.get("Codes");
    $scope.isShow = true;
    getJob();

    //method to get Job detals by JobId
    function getJob() {
        $scope.loadPanelVisible = true;
        $scope.loadPanelMessage = "Fetching vacancy details..."
        commonService.getJobById($scope.JobId).then(function (result) {
            $scope.job = result.data.results;
            var Skills = $scope.job.lstSkills;

            angular.forEach($scope.Codes, function (item1) {
                angular.forEach(Skills, function (item2) {
                    if (item1.Id == item2.SkillId) {

                        $scope.Skills.push(item1);
                    }
                });
            });

            if($scope.job.Status == 1){
                $scope.isShow = false;
            }
            $scope.loadPanelVisible = false;
        }, function (error) {
            $scope.loadPanelVisible = false;
            Flash.create('danger', error);
            return;
        });
    }

    // method to close a job vacancy
    $scope.close = function () {
        $scope.job.Status = 1;
        $scope.loadPanelVisible = true;
        $scope.loadPanelMessage = "Updating vacancy details";

        commonService.updateJob($scope.job).then(function (result) {
            if (result.data.status == 1) {
                $scope.loadPanelVisible = false;
                Flash.create("success", "Vacancy detailss updated successfully");
                $scope.isShow = false;
            }
            else {
                $scope.loadPanelVisible = false;
                Flash.create("danger", result.data.message);
                return;
            }

        }, function (error) {
            $scope.loadPanelVisible = false;
            Flash('danger', "Error: " + error);
            return;
        });
    };

    //dialogue box to confirm close job action
    $scope.showConfirm = function (ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Would you like to close the job vacancy?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Confirm')
              .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {
            $scope.close();
        }, function () {
            //
        });
    };
}]);

