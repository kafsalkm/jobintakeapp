﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 124 June 2017
    Description : Controller to handle Achievement page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("ClientCurrJobCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash', 'localStorageService', '$linq','appSettings',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, localStorageService,  $linq, appSettings) {
    var vm = this;

    vm.skills = {};
    $scope.job = {
        jobTitle : "",
        jobLocation : 0,
        jobSkills : [],
        salaryFrom: 0,
        salaryTo: 0,
        jobSpec: ""
        };
    //development stack
    $scope.Codes = localStorageService.get("Codes");

    $scope.locations = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.location);
                        }).ToArray();
    $scope.jobLocation = $scope.locations[0].Description;
    $scope.skills = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.skill);
                        }).ToArray();

    $scope.submit = function () {
        alert(JSON.stringify(job));
    };
} ]);

