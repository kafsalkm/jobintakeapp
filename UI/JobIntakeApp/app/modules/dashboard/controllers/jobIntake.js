﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Controller to handle job intake page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("ClientJobIntakeCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash',
    'localStorageService', '$linq', 'appSettings', 'commonService',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, localStorageService,  $linq, appSettings, commonService) {
    var vm = this;

    vm.skills = {};
    $rootScope.navbar = true;
    $scope.loadPanelVisible = false;
    $scope.ClientId = localStorageService.get("ClientId");
    localStorageService.set("ClientDetails", $scope.ClientDetails);

    $scope.job = {
        JobTitle: "",
        NoRequired: "",
        Location : "",
        lstSkills: [],
        SalaryFrom: "",
        SalaryTo: "",
        JobSpecification: "",
        OverallExp: "",
        RelaventExp: "",
        ClientId : $scope.ClientId
        };
    //development stack
    $scope.Codes = localStorageService.get("Codes");

    $scope.locations = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.location);
                        }).ToArray();
    $scope.Location = $scope.locations[0].Description;
    $scope.skills = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.skill);
                        }).ToArray();

    $scope.submit = function () {
        
        document.getElementById("sbt").disabled = true;
        $scope.loadPanelVisible = true;
        $scope.loadPanelMessage = "Submitting..."
        var isValid = validateUserInputs();
        if (isValid) {
            commonService.insertJob($scope.job).then(function (result) {
                document.getElementById("sbt").disabled = true;
                if (result.data.status == 1) {
                    $scope.loadPanelVisible = false;
                    Flash.create('success', "Success: " + result.data.message);
                    $state.go('app.ClientVacancy');
                }
                else {
                    $scope.loadPanelVisible = false;
                    Flash.create('danger', "Error: " + result.data.messag);
                    return;
                }

            }, function (error) {
                $scope.loadPanelVisible = false;
                Flash.create('danger', "Error: " + error);
                return;
            });
        }

        else {
            $scope.loadPanelVisible = false;
            Flash.create('danger', "Warning: Please enter all mandatory details");
            return;
        }
    };

    $scope.validateTitle = function () {
        if ($scope.job.JobTitle == null || $scope.job.JobTitle == "") {
            document.getElementById("jobTilte").style.border = "2px solid red";
        }
        else {
            document.getElementById("jobTilte").style.border = "1px solid #d2d6de";
        }
    };

    $scope.validateNo = function () {
        if ($scope.job.NoRequired == null || $scope.job.NoRequired == 0 || isNaN($scope.job.NoRequired)) {
            $scope.job.NoRequired = 0;
            document.getElementById("noRequired").style.border = "2px solid red";
        }
        else {
            document.getElementById("noRequired").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateLocation = function () {
        if ($scope.job.Location == null || $scope.job.Location == 0) {
            document.getElementById("jobLocation").style.border = "2px solid red";  
        }
        else {
            document.getElementById("jobLocation").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateSalary = function () {
        var salFr = Number($scope.job.SalaryFrom);
        var salTo = Number($scope.job.SalaryTo);

        if (isNaN($scope.job.SalaryFrom)) {
            $scope.job.SalaryFrom = 0;
            document.getElementById("salaryFrom").style.border = "2px solid red";
        }
        if (isNaN($scope.job.SalaryTo)) {
            $scope.job.SalaryTo = 0;
            document.getElementById("salaryTo").style.border = "2px solid red";
        }
        if ((salFr > salTo) && ($scope.job.SalaryTo != "")) {
            $scope.job.SalaryTo = 0;
            Flash.create('warning', "Salary To cannot be lesser than Slary From")
            document.getElementById("salaryFrom").style.border = "2px solid red";
            document.getElementById("salaryTo").style.border = "2px solid red";
        }
        else {
            document.getElementById("salaryFrom").style.border = "1px solid #d2d6de";
            document.getElementById("salaryTo").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateSkill = function () {
 
        if ($scope.job.lstSkills == null || $scope.job.lstSkills.length == 0) {
            document.getElementById("skillset").style.border = "2px solid red";
        }
        else {
            document.getElementById("skillset").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateJobSpec = function () {
        if ($scope.job.JobSpecification == null || $scope.job.JobSpecification == "") {
            document.getElementById("jobSpec").style.border = "2px solid red";
        }
        else {
            document.getElementById("jobSpec").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateExperience = function () {

        var ovrExp = Number($scope.job.OverallExp);
        var relExp = Number($scope.job.RelaventExp);
        if (isNaN($scope.job.OverallExp)) {
            $scope.job.OverallExp = 0;
            document.getElementById("ovrExp").style.border = "2px solid red";
        }
        else if (isNaN($scope.job.RelaventExp)) {
            $scope.job.RelaventExp = 0;
            document.getElementById("relExp").style.border = "2px solid red";
        }
        else if ((relExp > ovrExp) && ($scope.job.RelaventExp !="")) {
            Flash.create('warning', "Warning: Overall experience cannot be lesser than relvent experience!")
            $scope.job.RelaventExp = 0;
            document.getElementById("ovrExp").style.border = "2px solid red";
            document.getElementById("relExp").style.border = "2px solid red";
        }
        else {
            document.getElementById("ovrExp").style.border = "1px solid #d2d6de";
            document.getElementById("relExp").style.border = "1px solid #d2d6de";
        }
    }

    function validateUserInputs() {
        if($scope.job.JobTitle == null || $scope.JobTitle == "" || $scope.job.NoRequired == 0 || $scope.job.Location == 0 ||
            $scope.job.lstSkills == null || $scope.job.JobSpecification == null || $scope.job.JobSpecification == "") {
            
            $scope.validateTitle();
            $scope.validateNo();
            $scope.validateLocation();
            $scope.validateSkill();
            $scope.validateJobSpec();
            return false;
        }
        else {
            return true;
        }
    }
} ]);

