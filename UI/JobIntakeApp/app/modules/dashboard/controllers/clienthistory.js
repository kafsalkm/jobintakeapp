﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 124 June 2017
    Description : Controller to handle Achievement page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("ClientHistoryCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash',
function ($rootScope, $scope, $state, $location, dashboardService, Flash) {
    
    $scope.loans = [
    { title: 'Car Loan1', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan2', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan3', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan4', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan5', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan6', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan7', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan8', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan9', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan10', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" },
{ title: 'Car Loan', id: 1, amount: '250000', interest: '8%', duration: '48 months', accountnumber: "scb123456", status: "active" },

{ title: 'Personal Loan', id: 2, amount: '100000', interest: '12%', duration: '24 months', accountnumber: "icici45896", status: "completed" },

{ title: 'House Loan', id: 3, amount: '500000', interest: '7%', duration: '60 months', accountnumber: "sbc12356", status: "pending" }
    ];

    $scope.filteredTodos = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 5;
    $scope.maxSize = 5;


    $scope.numPages = function () {
        return Math.ceil($scope.loans.length / $scope.numPerPage);
    };

    $scope.$watch('currentPage + numPerPage', function () {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;

        $scope.filteredloans = $scope.loans.slice(begin, end);
    });


} ]);

