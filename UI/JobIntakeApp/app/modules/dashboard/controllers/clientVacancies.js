﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Controller to handle Client Vacancy page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("ClientVacancyCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash', 'commonService', '$linq', 'localStorageService',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, commonService, $linq, localStorageService) {
    
    $rootScope.navbar = true;
    $scope.loadPanelVisible = false;
    $scope.ClientId = localStorageService.get("ClientId");
    getJobs();
    $scope.filteredTodos = [];
    $scope.currentPage = 1;
    $scope.currentPage1 = 1;
    $scope.numPerPage = 5;
    $scope.maxSize = 5;
    $scope.jobs = [];

    // get jobs
    function getJobs() {

        $scope.loadPanelVisible = true;
        $scope.loadPanelMessage = "Loading";
        commonService.fetchJobsByClientId($scope.ClientId).then(function (result) {
            $scope.alljobs = result.data.results;
            $scope.loadPanelVisible = false;

            $scope.jobs = $linq.Enumerable().From($scope.alljobs)
                .Where(function (x) {
                return (x.Status == 0);
            }).ToArray();

            $scope.closedjobs = $linq.Enumerable().From($scope.alljobs)
                .Where(function (x) {
                    return (x.Status == 1);
                }).ToArray();

            $scope.numPages();
            $scope.numPages1();

            $scope.$watch('currentPage + numPerPage', function () {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;

                $scope.filteredjobs = $scope.jobs.slice(begin, end);
            });

            $scope.$watch('currentPage1 + numPerPage', function () {
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage)
                , end1 = begin1 + $scope.numPerPage;

                $scope.filteredjobs = $scope.closedjobs.slice(begin, end);
            });
        }, function (error) {
            Flash('danger', "Error: " + error);
        });
    }

    $scope.numPages = function () {
        return Math.ceil($scope.jobs.length / $scope.numPerPage);
    };

    $scope.numPages1 = function () {
        return Math.ceil($scope.closedjobs.length / $scope.numPerPage);
    };

    //view job
    $scope.viewJob = function (event) {
        localStorageService.set("jobId", event.target.id);
        $state.go('app.jobDetails');
    }
} ]);

