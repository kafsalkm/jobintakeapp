﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Global configuration defined here
    
    Change Log
    s.no      date    author     description     


 ===========================================================*/

var appConfig = {
    title: "Corporate Directory",
    lang: "en",
    dateFormat: "mm/dd/yy",
    apiBase: 'http://13.59.77.191:8080/api/',
    theme: 'skin-blue-light',
    layout: "fixed",
    location: "Location",
    skill: "Skill"
};