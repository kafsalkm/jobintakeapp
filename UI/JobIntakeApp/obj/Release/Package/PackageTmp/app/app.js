/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : script file to handle angular module initiation and routing
    Change Log
    s.no      date    author     description     

 ===========================================================*/

var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'flash','LocalStorageModule','angular-linq','dx',
    //main modules
    'login', 'dashboard', 'ngMaterial']);


app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $modalInstance) {

    //IdleScreenList
    $stateProvider
       .state('app', {
           url: '/app',
           templateUrl: 'app/common/app.html',
           controller: 'appCtrl',
           controllerAs: 'vm',
           data: {
               pageTitle: ''
           }
       });

    $urlRouterProvider.otherwise('app');
}]);

// set global configuration of application and it can be accessed by injecting appSettings in any modules
app.constant('appSettings', appConfig);
