﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Controller to handle Login module
    Change Log
    s.no      date    author     description     


 ===========================================================*/

login.controller("loginCtrl", ['$rootScope', '$scope', '$state', '$location', 'loginService', 'Flash', 'apiService', 'localStorageService', '$linq',
function ($rootScope, $scope, $state, $location, loginService, Flash, apiService, localStorageService, $linq) {
    var vm = this;

    vm.getUser = {};
    vm.setUser = {};
    vm.signIn = true;

    //access login
    vm.login = function (data) {
        $scope.loadbarvisible = true;

        // add user authentication method here

        $state.go('app');
    };

} ]);

