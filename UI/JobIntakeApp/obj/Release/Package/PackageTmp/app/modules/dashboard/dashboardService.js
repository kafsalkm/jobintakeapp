﻿/*==========================================================
   Author      : Kafsal KM
   Date Created: 24 Jaune 2017
   Description : To handle the service for Dashboard module
   
   Change Log
   s.no      date    author     description     
===========================================================*/


dashboard.service('dashboardService', ['$http', '$q', 'Flash', 'apiService', 'appSettings', function ($http, $q, Flash, apiService, appSettings) {

    var dashboardService = {};
    var apiBase = appSettings.apiBase;

    //service to communicate with users model to verify login credentials
    var accessLogin = function (parameters) {
        var deferred = $q.defer();
        apiService.get("users", parameters).then(function (response) {
            if (response)
                deferred.resolve(response);
            else
                deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
        },
            function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    //service to fetch codes list
    var getCodes = function () {

        return $http.get(apiBase + "/Home/FetchAllCodes").then(function (result) {
            return result;
        }, function (error) {
            return error;
        });

    };

    //service to fetch client details
    var getClientDetails = function (ClientId) {

        return $http.get(apiBase + "/Home/FetchClientByClientId?ClientId=" + ClientId).then(function (result) {
            return result;
        }, function (error) {
            return error;
        });

    };

    dashboardService.accessLogin = accessLogin;
    dashboardService.getCodes = getCodes;
    dashboardService.getClientDetails = getClientDetails;
    return dashboardService;

}]);
