﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Controller to handle job intake page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("ClientJobIntakeCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash',
    'localStorageService', '$linq', 'appSettings', 'commonService',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, localStorageService,  $linq, appSettings, commonService) {
    var vm = this;

    vm.skills = {};
    $rootScope.navbar = true;
    $scope.ClientId = localStorageService.get("ClientId");
    localStorageService.set("ClientDetails", $scope.ClientDetails);

    $scope.job = {
        JobTitle: "",
        NoRequired: "",
        Location : "",
        lstSkills: [],
        SalaryFrom: "",
        SalaryTo: "",
        JobSpecification: "",
        ClientId : $scope.ClientId
        };
    //development stack
    $scope.Codes = localStorageService.get("Codes");

    $scope.locations = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.location);
                        }).ToArray();
    $scope.Location = $scope.locations[0].Description;
    $scope.skills = $linq.Enumerable().From($scope.Codes)
                        .Where(function (x) {
                            return (x.Code == appSettings.skill);
                        }).ToArray();

    $scope.submit = function () {
        
        var isValid = validateUserInputs();
        if (isValid) {
            commonService.insertJob($scope.job).then(function (result) {

                if (result.data.status == 1) {
                    Flash.create('success', "Success: " + result.data.message);
                    $state.go('app.ClientVacancy');
                }
                else {
                    Flash.create('danger', "Error: " + result.data.messag);
                    return;
                }

            }, function (error) {
                Flash.create('danger', "Error: " + error);
                return;
            });
        }

        else {
            Flash.create('danger', "Warning: Please enter all mandatory details");
            return;
        }
    };

    $scope.validateTitle = function () {
        if ($scope.job.JobTitle == null || $scope.job.JobTitle == "") {
            document.getElementById("jobTilte").style.border = "2px solid red";
        }
        else {
            document.getElementById("jobTilte").style.border = "1px solid #d2d6de";
        }
    };

    $scope.validateNo = function () {
        if ($scope.job.NoRequired == null || $scope.job.NoRequired == 0 || isNaN($scope.job.NoRequired)) {
            $scope.job.NoRequired = 0;
            document.getElementById("noRequired").style.border = "2px solid red";
        }
        else {
            document.getElementById("noRequired").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateLocation = function () {
        if ($scope.job.Location == null || $scope.job.Location == 0) {
            document.getElementById("jobLocation").style.border = "2px solid red";  
        }
        else {
            document.getElementById("jobLocation").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateSalary = function () {
        if (isNaN($scope.job.SalaryFrom)) {
            $scope.job.SalaryFrom = 0;
            document.getElementById("salaryFrom").style.border = "2px solid red";
        }
        if (isNaN($scope.job.SalaryTo)) {
            $scope.job.SalaryTo = 0;
            document.getElementById("salaryTo").style.border = "2px solid red";
        }
        if ($scope.job.SalaryFrom > $scope.job.SalaryTo) {
            $scope.job.SalaryTo = 0;
            document.getElementById("salaryFrom").style.border = "2px solid red";
            document.getElementById("salaryTo").style.border = "2px solid red";
        }
        else {
            document.getElementById("salaryFrom").style.border = "1px solid #d2d6de";
            document.getElementById("salaryTo").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateSkill = function () {
 
        if ($scope.job.lstSkills == null || $scope.job.lstSkills.length == 0) {
            document.getElementById("skillset").style.border = "2px solid red";
        }
        else {
            document.getElementById("skillset").style.border = "1px solid #d2d6de";
        }
    }

    $scope.validateJobSpec = function () {
        if ($scope.job.JobSpecification == null || $scope.job.JobSpecification == "") {
            document.getElementById("jobSpec").style.border = "2px solid red";
        }
        else {
            document.getElementById("jobSpec").style.border = "1px solid #d2d6de";
        }
    }

    function validateUserInputs() {
        if($scope.job.JobTitle == null || $scope.JobTitle == "" || $scope.job.NoRequired == 0 || $scope.job.Location == 0 ||
            $scope.job.lstSkills == null || $scope.job.JobSpecification == null || $scope.job.JobSpecification == "") {
            
            $scope.validateTitle();
            $scope.validateNo();
            $scope.validateLocation();
            $scope.validateSkill();
            $scope.validateJobSpec();
            return false;
        }
        else {
            return true;
        }
    }
} ]);

