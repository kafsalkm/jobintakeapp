﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 124 June 2017
    Description : Controller to handle menu page
    Change Log
    s.no      date    author     description     


 ===========================================================*/

dashboard.controller("menuCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash',
function ($rootScope, $scope, $state, $location, dashboardService, Flash) {
    
    $rootScope.navbar = false;

    $scope.register = function () {
        // ethod here
    }
    
    $scope.login = function () {
        $state.go('app.ClientVacancy');
    }
}]);

