﻿/*==========================================================
   Author      : Kafsal KM
   Date Created: 24 June 2017
   Description : To handle the service for Dashboard module
   
   Change Log
   s.no      date    author     description     
===========================================================*/


dashboard.service('commonService', ['$http', '$q', 'appSettings', function ($http, $q, appSettings) {

    var commonService = {};
    var apiBase = appSettings.apiBase;

    //=================================Add new Job===========================================
    var insertJob = function (job) {

        var objJob = {
            "Job": job
        }

        return $http.post(apiBase + "Home/InsertJob", objJob).then(function (result) {
            return result;
        },
        function (error) {
            return error;
        });
    }

    //==============================Fetch All Jobs by Client Id===============================
    var fetchJobsByClientId = function (ClientId) {
        
        return $http.get(apiBase + "Home/GetJobsByClientId?ClientId=" +  ClientId).then(function (result) {
            return result;
        },
        function (error) {
            return error;
        });
    }

    //==============================Fetch Job by Job Id===============================
    var getJobById = function (JobId) {

        return $http.get(apiBase + "Home/GetJobById?JobId=" + JobId).then(function (result) {
            return result;
        },
        function (error) {
            return error;
        });
    }

    //==============================Fetch Job by Job Id===============================
    var updateJob = function (Job) {

        obj = {
            "Job": Job 
        }
        return $http.post(apiBase + "Home/UpdateJob", obj).then(function (result) {
            return result;
        },
        function (error) {
            return error;
        });
    }

    commonService.insertJob = insertJob;
    commonService.fetchJobsByClientId = fetchJobsByClientId;
    commonService.getJobById = getJobById;
    commonService.updateJob = updateJob;
    return commonService;

}]);
