﻿/*==========================================================
    Author      : Kafsal KM
    Date Created: 24 June 2017
    Description : Base for Dashboard Application module
    
    Change Log
    s.no      date    author     description     
    

 ===========================================================*/

var dashboard = angular.module('dashboard', ['ui.router', 'ngAnimate', 'ngMaterial', 'dx', 'ui.bootstrap', 'ngMaterial']);


dashboard.config(["$stateProvider", function ($stateProvider) {

    // job intake view routing
    $stateProvider.state('app.clientjobintake', {
        url: '/addvacancy',
        templateUrl: 'app/modules/dashboard/views/clientjobintake.html',
        controller: 'ClientJobIntakeCtrl',
        controllerAs: 'vm'
    });

    //menu view routing
    $stateProvider.state('app.menu', {
        url: '/home',
        templateUrl: 'app/modules/dashboard/views/metromenu.html',
        controller: 'menuCtrl',
        controllerAs: 'vm'
    });

    //client vacancy listing view routing
    $stateProvider.state('app.ClientVacancy', {
        url: '/myvacancies',
        templateUrl: 'app/modules/dashboard/views/clientVacancies.html',
        controller: 'ClientVacancyCtrl'
    });

    //Job details view routing
    $stateProvider.state('app.jobDetails', {
        url: '/jobdetails',
        templateUrl: 'app/modules/dashboard/views/jobDetail.html',
        controller: 'JobDetailCtrl',
        controllerAs: 'vm'
    });

   
}]);

dashboard.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});