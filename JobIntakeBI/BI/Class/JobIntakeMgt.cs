﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobIntake.DataBaseManager;
using JobIntake.DataObject;

namespace JobIntakeBI
{
    public class JobIntakeMgt : IJobIntakeMgt
    {
        /// <summary>
        /// To fetch the Job details by JobId
        /// </summary>
        /// <param name="CandidateId"></param>
        /// <returns>Job</returns>
        public Job FetchJobByJobId(SessionDetails sessDetls, long JobId)
        {
            try
            {
                IJobIntakeDAL bizJobIntakeDal = new JobIntakeDAL(sessDetls);
                Job objJobDetails = bizJobIntakeDal.FetchJobByJobId(JobId);
                List<GenericCode> lstCodes = bizJobIntakeDal.FetchAllCodes();
                objJobDetails.LocationName = lstCodes.Find(x => x.Id == objJobDetails.Location).Description;
                if (objJobDetails != null)
                {
                    objJobDetails.lstSkills = bizJobIntakeDal.FetchSkillyJobId(JobId);
                }

                return objJobDetails;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in fetch job details", ex);
            }
        }

        /// <summary>
        /// To fetch the LstJob details by ClientId
        /// </summary>
        /// <param name="CandidateId"></param>
        /// <returns>lstJob</returns>
        public List<Job> FetchJobsByClientId(SessionDetails sessDetls, long ClientId)
        {
            try
            {
                IJobIntakeDAL bizJobIntakeDal = new JobIntakeDAL(sessDetls);
                List<Job> lstJobDetails = bizJobIntakeDal.FetchJobsByClientId(ClientId);
                List<GenericCode> lstCodes  = bizJobIntakeDal.FetchAllCodes();
                foreach (Job obj in lstJobDetails)
                {
                    obj.LocationName = lstCodes.Find(x => x.Id == obj.Location).Description;
                }
                return lstJobDetails;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in fetch jobs details", ex);
            }
        }

        /// <summary>
        /// To Insert the Job details into db
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertJob(SessionDetails sessDetls, Job objJob)
        {
            try
            {
                IJobIntakeDAL bizJobIntakeDal = new JobIntakeDAL(sessDetls);
                ErrorDetails err = bizJobIntakeDal.InsertJob(objJob);
                if (err == null)
                {
                    foreach (Skill objSkill in objJob.lstSkills)
                    {
                        objSkill.SkillId = objSkill.Id;
                        objSkill.JobId = objJob.Id;
                        err = bizJobIntakeDal.InsertJobSkill(objSkill);
                    }
                }
                return err;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in insert job details", ex);
            }
        }

        /// <summary>
        /// To Insert the Client details into db
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertClient(SessionDetails sessDetls, Client objClient)
        {
            try
            {
                IJobIntakeDAL bizJobIntakeDal = new JobIntakeDAL(sessDetls);
                ErrorDetails err = bizJobIntakeDal.InsertContact(objClient.ContactDetails);
                if (err == null)
                {
                    objClient.Contact = objClient.ContactDetails.Id;
                    err = bizJobIntakeDal.InsertClient(objClient);
                }
                return err;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in insert client details", ex);
            }
        }

        /// <summary>
        /// To Fetch the All Generic Codes
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>lstCodes</returns>
        public List<GenericCode> FetchAllCodes(SessionDetails sessDetls)
        {
            try
            {
                IJobIntakeDAL bizJobIntakeDal = new JobIntakeDAL(sessDetls);
                return bizJobIntakeDal.FetchAllCodes();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in fetching codes details", ex);
            }
        }

        /// <summary>
        /// To Fetch the Client details
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>Client</returns>
        public Client FetchClientByClientId(SessionDetails sessDetls, long ClientId)
        {
            try
            {
                IJobIntakeDAL iJobDal = new JobIntakeDAL(sessDetls);
                Client objClient = iJobDal.FetchClientByClientId(ClientId);
                if (objClient != null)
                {
                    objClient.ContactDetails = iJobDal.FetchContactByContactId(objClient.Contact);
                }
                return objClient;
            }
            catch (Exception ex)
            {
                throw new Exception("Something went wrong: " + ex.Message);
            }
        }

        /// <summary>
        /// To Update the Job details
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails UpdateJobDetails(SessionDetails sessDetls, Job objJob)
        {
            try{
                IJobIntakeDAL iJobDal = new JobIntakeDAL(sessDetls);
                return iJobDal.UpdateJobDetails(objJob);
            }
            catch(Exception ex){
                throw new Exception("Something went wrong in update job method: " + ex.Message);
            }
        }
    }
}
