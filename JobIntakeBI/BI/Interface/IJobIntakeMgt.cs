﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobIntake.DataObject;

namespace JobIntakeBI
{
    public interface IJobIntakeMgt
    {
        Job FetchJobByJobId(SessionDetails sessDetls, long JobId);
        List<Job> FetchJobsByClientId(SessionDetails sessDetls, long ClientId);
        List<GenericCode> FetchAllCodes(SessionDetails sessDetls);
        Client FetchClientByClientId(SessionDetails sessDetls, long ClientId);
        ErrorDetails InsertJob(SessionDetails sessDetls, Job objJob);
        ErrorDetails InsertClient(SessionDetails sessDetls, Client objClient);
        ErrorDetails UpdateJobDetails(SessionDetails sessDetls, Job objJob);
    }
}
