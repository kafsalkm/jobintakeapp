﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobIntake.DataObject;

namespace JobIntakeBI
{
    public interface IJobIntakeDAL
    {
        Job FetchJobByJobId(long JobId);
        List<Skill> FetchSkillyJobId(long JobId);
        List<Job> FetchJobsByClientId(long ClientId);
        Client FetchClientByClientId(long ClientId);
        Contact FetchContactByContactId(long ContactId);
        List<GenericCode> FetchAllCodes();
        ErrorDetails InsertJob(Job objJob);
        ErrorDetails InsertClient(Client objClient);
        ErrorDetails UpdateJobDetails(Job objJob);
        ErrorDetails InsertContact(Contact objContact);
        ErrorDetails InsertJobSkill(Skill objSkill);
    }
}
