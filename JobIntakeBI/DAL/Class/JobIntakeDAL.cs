﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobIntake.DataBaseManager;
using JobIntake.DataObject;

namespace JobIntakeBI
{
    public class JobIntakeDAL : BaseDataAccessLayer, IJobIntakeDAL
    {

        #region Constructor

        public JobIntakeDAL(SessionDetails sessionDtls)
            : base(null)
        {
            sesDtls = sessionDtls;
        }

        public JobIntakeDAL(IDbTransaction sqlTxn, SessionDetails sessionDtls)
            : base(sqlTxn)
        {
            sesDtls = sessionDtls;
        }

        #endregion

        #region Fetch Methods

        /// <summary>
        /// To fetch the Job details by JobId
        /// </summary>
        /// <param name="JobId"></param>
        /// <returns>Job</returns>
        public Job FetchJobByJobId(long JobId)
        {
            Dictionary<string, object> whereConditions = new Dictionary<string, object>();
            whereConditions[DALConstants.ID] = JobId;
            return FetchSingleRecord<Job>(null, whereConditions);
        }

        /// <summary>
        /// To fetch the Skill details by JobId
        /// </summary>
        /// <param name="Job">Id</param>
        /// <returns>Job</returns>
        public List<Skill> FetchSkillyJobId(long JobId)
        {
            Dictionary<string, object> whereConditions = new Dictionary<string, object>();
            whereConditions[DALConstants.JOB_ID] = JobId;
            return FetchRecord<Skill>(null, whereConditions);
        }

        /// <summary>
        /// To fetch the Job list details by ClientId
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns>lstJob</returns>
        public List<Job> FetchJobsByClientId(long ClientId)
        {
            Dictionary<string, object> whereConditions = new Dictionary<string, object>();
            whereConditions[DALConstants.CLIENT_ID] = ClientId;
            return FetchRecord<Job>(null, whereConditions);
        }

        /// <summary>
        /// To fetch the Client details by ClientId
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns>Client</returns>
        /// 
        public Client FetchClientByClientId(long ClientId)
        {
            Dictionary<string, object> whereConditions = new Dictionary<string, object>();
            whereConditions[DALConstants.ID] = ClientId;
            return FetchSingleRecord<Client>(null, whereConditions);
        }

        /// <summary>
        /// To fetch the Contact details by ClientId
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns>Contact</returns>
        /// 
        public Contact FetchContactByContactId(long ContactId)
        {
            Dictionary<string, object> whereConditions = new Dictionary<string, object>();
            whereConditions[DALConstants.ID] = ContactId;
            return FetchSingleRecord<Contact>(null, whereConditions);
        }

        /// <summary>
        /// To fetch all Generic Codes
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns>lstGenericCode</returns>
        /// 
        public List<GenericCode> FetchAllCodes()
        {
            return FetchAllRecords<GenericCode>(null);
        }

         #endregion

        #region Insert Methods

        /// <summary>
        /// To Insert the Job details into db
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertJob(Job objJob)
        {
            object[] objRes = new object[2];
            objRes = InsertRecord(objJob, 0);
            objJob.Id = Convert.ToInt64(objRes[1]);
            return objRes[1] as ErrorDetails;
        }

        /// <summary>
        /// To Insert the Ctlien details into db
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertClient(Client objClient)
        {
            object[] objRes = new object[2];
            objRes = InsertRecord(objClient, 0);
            objClient.Id = Convert.ToInt64(objRes[1]);
            return objRes[1] as ErrorDetails;
        }

        /// <summary>
        /// To Insert the Contact details into db
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertContact(Contact objContact)
        {
            object[] objRes = new object[2];
            objRes = InsertRecord(objContact, 0);
            objContact.Id = Convert.ToInt64(objRes[1]);
            return objRes[1] as ErrorDetails;
        }

        /// <summary>
        /// To Insert the JobSkill details into db
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails InsertJobSkill(Skill objSkill)
        {
            object[] objRes = new object[2];
            objRes = InsertRecord(objSkill, 0);
            objSkill.Id = Convert.ToInt64(objRes[1]);
            return objRes[1] as ErrorDetails;
        }
        #endregion

        #region Update Methods

        /// <summary>
        /// Update Job Details
        /// </summary>
        /// <param name="">Object</param>
        /// <returns>ErrorDetails</returns>
        public ErrorDetails UpdateJobDetails(Job objJob)
        {
            return UpdateRecord(objJob);
        }

        #endregion

    }
}
